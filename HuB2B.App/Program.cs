﻿using HuB2B.WinService.Data;
using HuB2B.WinService.Executores;
using HuB2B.WinService.WebApi;
using System;
using System.Threading.Tasks;


namespace HuB2B.App
{
    class Program
    {
        static readonly HuB2BDataContext _ctx = new HuB2BDataContext();

        static void Main(string[] args)
        {
            _ctx.Database.CommandTimeout = 300;

            string ChaveAuth = _ctx.HbParametros.Find("CHAVE_AUTH").VALOR;
            string IdTenant = _ctx.HbParametros.Find("ID_TENANT").VALOR;
            Uri baseAddress = new Uri(_ctx.HbParametros.Find("BASE_ADDRESS").VALOR);
            
            Task.Run(async () =>
            {
            // => PROCESSO PARA TRAZER RESULTADO DE CADA LINHA EXECUTADA 
              
              //await apiPedidos.PedidosID(baseAddress, IdTenant, ChaveAuth);
              //await apiPedidos.Pedidos(baseAddress, IdTenant, ChaveAuth);                                
              //await apiPedidos.ConfirmaDownload(baseAddress, IdTenant, ChaveAuth);
              //await apiPedidos.AtualizarFaturamento(baseAddress, IdTenant, ChaveAuth);
              await apiEstoque.AtualizarEstoque(baseAddress, IdTenant, ChaveAuth);   

            }).Wait();  

            OCMS.AuBresilXML(_ctx.HbParametros.Find("FTP_AUBRESIL").VALOR, @"ORDER_Multiplus-OAB.xml");
            OCMS.ProvenceXML(_ctx.HbParametros.Find("FTP_PROVENCE").VALOR, @"ORDER_Multiplus-OEP.xml");

            
            SFTP.FileUpload(_ctx.HbParametros.Find("FTP_AUBRESIL").VALOR, @"10.33.17.4", 22, "am03svc-ftpecomm", "ecoFmmeTrceP", @"\\Multiplus\Multiplus_Orders_OAB\");
            SFTP.FileUpload(_ctx.HbParametros.Find("FTP_PROVENCE").VALOR, @"10.33.17.4", 22, "am03svc-ftpecomm", "ecoFmmeTrceP", @"\\Multiplus\Multiplus_Orders_OEP\");


            if(DateTime.Now.DayOfWeek == DayOfWeek.Friday && DateTime.Now.Hour > 9 && DateTime.Now.Hour < 10)
            {
                Relatorio.Enviar();
            }

           // OCMS.ProvenceImportarDadosCSV(@"D:\", "OrdersMup_OCMS.csv"); // PROVENCE
           // OCMS.AuBresilImportarDadosCSV(@"D:\", "OrdersMup_OBCMS.csv"); // BRESIL

        }

    }
}