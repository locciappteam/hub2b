﻿using Renci.SshNet;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace HuB2B.WinService.Executores
{
    public class SFTP
    {
        public static void FileUpload(string PathXml, string Host, int Port, string Username, string Password, string PathFTP)
        {
            string[] Files = Directory.GetFiles(PathXml, "*.xml");

            if (Files.Any())
            {
                using (SftpClient sftpClient = new SftpClient(Host, Port, Username, Password))
                {
                    sftpClient.Connect();

                    if (sftpClient.IsConnected)
                    {
                        sftpClient.ChangeDirectory(PathFTP);

                        Log.Write(String.Format($"{DateTime.Now} : FTP - Conexão estabelecida"));

                        foreach (var file in Files)
                        {
                            using (FileStream fileStream = new FileStream(file, FileMode.Open))
                            {
                                sftpClient.UploadFile(fileStream, Path.GetFileName(file));

                                Log.Write(String.Format($"{DateTime.Now} ::: Arquivo {Path.GetFileName(file)} importado."));
                            }
                        }
                        sftpClient.Disconnect();

                        Log.Write(String.Format($"{DateTime.Now} : FTP - Conexão encerrada"));
                    }
                    else
                    {
                        Log.Write(String.Format($"{DateTime.Now} : FTP - Conexão NÃO foi estabelecida"));
                    }
                }
                foreach (var file in Files)
                {
                    if (!File.Exists(String.Format($@"{PathXml}Backup\{Path.GetFileName(file)}")))
                    {
                        File.Move(file, String.Format($@"{PathXml}Backup\{Path.GetFileName(file)}"));
                    }
                    else
                    {
                        File.Delete(String.Format($@"{PathXml}Backup\{Path.GetFileName(file)}"));
                        File.Move(file, String.Format($@"{PathXml}Backup\{Path.GetFileName(file)}"));
                    }
                }
            }
        }
    }
}
