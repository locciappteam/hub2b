﻿using HuB2B.WinService.Data;
using HuB2B.WinService.Entidades.Integracao;
using HuB2B.WinService.Entidades.Pedidos;
using HuB2B.WinService.Excel;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml;

namespace HuB2B.WinService.Executores
{
    public class OCMS
    {
        private static HuB2BDataContext _ctx = new HuB2BDataContext();
        private static ViewsDataContext _views = new ViewsDataContext();

        public static void AuBresilXML(string Path, string NomeArquivo)
        {
            List<HB_PEDIDO_CAPA> PedidosBresil = (
                    from
                        a in _ctx.HbPedidoItem
                    join b in _ctx.MarcaProdutos on a.COD_PRODUTO equals b.COD_PRODUTO
                    where
                        a.HB_PEDIDO_CAPA.DATA_CANCELAMENTO == null
                        && a.XML_GERADO == false
                        && b.MARCAID == 1
                    select
                        a.HB_PEDIDO_CAPA
            ).Distinct().ToList();

            if (PedidosBresil.Any())
            {
                Log.Write(String.Format($"{DateTime.Now} : XML AuBresil - Gerando arquivo XML"));

                string Arquivo = String.Format($"{Path}{DateTime.Now.ToString("yyyyMMddhhmmss")}_{NomeArquivo}");
                try
                {
                    using (XmlWriter xml = XmlWriter.Create(Arquivo, new XmlWriterSettings { Indent = true, IndentChars = "  ", NewLineOnAttributes = false, OmitXmlDeclaration = true, Encoding = new UTF8Encoding(false) }))
                    {
                        xml.WriteStartDocument();
                        xml.WriteStartElement("ROOT");
                        xml.WriteStartElement("OrderMessageBatch");
                        xml.WriteAttributeString("batchNumber", "1");

                        foreach (var Pedido in PedidosBresil)
                        {
                            xml.WriteStartElement("hubOrder");
                            xml.WriteAttributeString("transactionID", (PedidosBresil.IndexOf(Pedido) + 1).ToString());
                            xml.WriteElementString("lineCount", Pedido.HB_PEDIDO_ITEMs.Count().ToString());
                            xml.WriteElementString("poNumber", Pedido.NUM_PEDIDO);
                            xml.WriteElementString("orderDate", Pedido.DATA_EMISSAO.ToString("yyyy-MM-dd"));
                            xml.WriteElementString("paymentMethod", "BOLETO");
                            xml.WriteElementString("tax", "0");
                            xml.WriteElementString("total", (Pedido.VALOR_TOTAL).ToString().Replace(',', '.')); //Não precisa abater o desconto. Já inclui o valor do frete
                            xml.WriteElementString("shippingCode", "Entrada REGULAR");

                            xml.WriteStartElement("shippingInfo");
                            xml.WriteElementString("name1", Pedido.HB_PEDIDO_ENDERECO.NOME_RECEBEDOR);
                            xml.WriteElementString("name2", "");
                            //xml.WriteElementString("name2", Pedido.HB_PEDIDO_ENDERECO.NOME_RECEBEDOR.Substring(Pedido.HB_PEDIDO_ENDERECO.NOME_RECEBEDOR.IndexOf(" "), Pedido.HB_PEDIDO_ENDERECO.NOME_RECEBEDOR.Length - Pedido.HB_PEDIDO_ENDERECO.NOME_RECEBEDOR.IndexOf(" ")).Trim());
                            xml.WriteElementString("address1", Pedido.HB_PEDIDO_ENDERECO.ENDERECO + ", " + Pedido.HB_PEDIDO_ENDERECO.NUMERO);
                            xml.WriteElementString("address2", Pedido.HB_PEDIDO_ENDERECO.COMPLEMENTO);
                            xml.WriteElementString("address3", Pedido.HB_PEDIDO_ENDERECO.BAIRRO);
                            xml.WriteElementString("city", Pedido.HB_PEDIDO_ENDERECO.CIDADE);
                            xml.WriteElementString("state", Pedido.HB_PEDIDO_ENDERECO.ESTADO);
                            xml.WriteElementString("country", Pedido.HB_PEDIDO_ENDERECO.PAIS);
                            xml.WriteElementString("postalCode", Pedido.HB_PEDIDO_ENDERECO.CEP.Replace("-", ""));
                            xml.WriteElementString("phoneNumber", Pedido.HB_PEDIDO_CLIENTE.TELEFONE);
                            xml.WriteEndElement();

                            xml.WriteStartElement("billingInfo");
                            xml.WriteElementString("name1", Pedido.HB_PEDIDO_CLIENTE.NOME);
                            xml.WriteElementString("name2", Pedido.HB_PEDIDO_CLIENTE.SOBRENOME);
                            xml.WriteElementString("address1", Pedido.HB_PEDIDO_ENDERECO.ENDERECO + ", " + Pedido.HB_PEDIDO_ENDERECO.NUMERO);
                            xml.WriteElementString("address2", Pedido.HB_PEDIDO_ENDERECO.COMPLEMENTO);
                            xml.WriteElementString("address3", Pedido.HB_PEDIDO_ENDERECO.BAIRRO);
                            xml.WriteElementString("city", Pedido.HB_PEDIDO_ENDERECO.CIDADE);
                            xml.WriteElementString("state", Pedido.HB_PEDIDO_ENDERECO.ESTADO);
                            xml.WriteElementString("country", Pedido.HB_PEDIDO_ENDERECO.PAIS);
                            xml.WriteElementString("postalCode", Pedido.HB_PEDIDO_ENDERECO.CEP.Replace("-", ""));
                            xml.WriteElementString("phoneNumber", Pedido.HB_PEDIDO_CLIENTE.TELEFONE);
                            xml.WriteEndElement();

                            xml.WriteStartElement("customerInfo");
                            xml.WriteElementString("CPF", Pedido.HB_PEDIDO_CLIENTE.NUM_DOCUMENTO);
                            xml.WriteElementString("name1", Pedido.HB_PEDIDO_CLIENTE.NOME);
                            xml.WriteElementString("name2", Pedido.HB_PEDIDO_CLIENTE.SOBRENOME);
                            xml.WriteElementString("address1", Pedido.HB_PEDIDO_ENDERECO.ENDERECO + ", " + Pedido.HB_PEDIDO_ENDERECO.NUMERO);
                            xml.WriteElementString("address2", Pedido.HB_PEDIDO_ENDERECO.COMPLEMENTO);
                            xml.WriteElementString("address3", Pedido.HB_PEDIDO_ENDERECO.BAIRRO);
                            xml.WriteElementString("city", Pedido.HB_PEDIDO_ENDERECO.CIDADE);
                            xml.WriteElementString("state", Pedido.HB_PEDIDO_ENDERECO.ESTADO);
                            xml.WriteElementString("country", Pedido.HB_PEDIDO_ENDERECO.PAIS);
                            xml.WriteElementString("postalCode", Pedido.HB_PEDIDO_ENDERECO.CEP.Replace("-", ""));
                            xml.WriteElementString("phoneNumber", Pedido.HB_PEDIDO_CLIENTE.TELEFONE);
                            xml.WriteElementString("email", String.Format($"{Pedido.NUM_PEDIDO}@{Pedido.CANAL_SITE}.com.br"));
                            xml.WriteEndElement();
                            xml.WriteElementString("custOrderNumber", Pedido.NUM_PEDIDO);
                            xml.WriteElementString("custOrderDate", Pedido.DATA_EMISSAO.ToString("yyyy-MM-dd"));

                            var t = _ctx.MarcaProdutos.AsNoTracking().Where(p=> p.MARCAID == 1).ToList();
                            var list = t.Where(p => p.COD_PRODUTO == "BROBVS100250").ToList();

                            int NUM_ITEM = 1;
                            foreach (var Item in Pedido.HB_PEDIDO_ITEMs.Where(a => (from marca in _ctx.MarcaProdutos where marca.MARCAID == 1 select marca.COD_PRODUTO).ToList().Contains(a.COD_PRODUTO)))
                            {
                                xml.WriteStartElement("lineItem");
                                xml.WriteElementString("lineItemId", NUM_ITEM.ToString());
                                xml.WriteElementString("orderLineNumber", NUM_ITEM.ToString());
                                xml.WriteElementString("qtyOrdered", Item.QUANTIDADE.ToString());
                                xml.WriteElementString("unitOfMeasure", "PECA");
                                xml.WriteElementString("description", "");
                                xml.WriteElementString("merchantProductId", Item.COD_PRODUTO);
                                xml.WriteElementString("merchantSKU", Item.COD_PRODUTO);
                                xml.WriteElementString("vendorSKU", Item.COD_PRODUTO);
                                xml.WriteElementString("unitPrice", (Item.VALOR_UNITARIO - Item.VALOR_DESCONTO).ToString().Replace(',', '.'));
                                xml.WriteElementString("unitCost", "BRL");
                                xml.WriteElementString("lineTax", "0");
                                xml.WriteElementString("lineSubTotal", Convert.ToString((Item.VALOR_UNITARIO * Item.QUANTIDADE) - Item.VALOR_DESCONTO).Replace(',', '.'));
                                xml.WriteEndElement();

                                Item.XML_GERADO = true;
                                _ctx.Entry(Item).State = EntityState.Modified;
                                _ctx.SaveChanges();

                                HB_PEDIDO_XML HbPedidoXml = new HB_PEDIDO_XML
                                {
                                    ITEMID = Item.ID,
                                    NOME_ARQUIVO = Arquivo,
                                    CADASTRADO_POR = "INTEGRACAO",
                                    DATA_CADASTRO = DateTime.Now,
                                    ALTERADO_POR = "INTEGRACAO",
                                    DATA_ALTERACAO = DateTime.Now
                                };

                                _ctx.Set<HB_PEDIDO_XML>().Add(HbPedidoXml);
                                _ctx.SaveChanges();

                                NUM_ITEM++;
                            }

                            if (Pedido.HB_PEDIDO_LOGISTICAs.Any() && Pedido.HB_PEDIDO_LOGISTICAs.Sum(a => a.VALOR_FRETE) > 0)
                            {
                                xml.WriteStartElement("lineItem");
                                xml.WriteElementString("lineItemId", NUM_ITEM.ToString());
                                xml.WriteElementString("orderLineNumber", NUM_ITEM.ToString());
                                xml.WriteElementString("qtyOrdered", "1");
                                xml.WriteElementString("unitOfMeasure", "PECA");
                                xml.WriteElementString("description", "FRETE");
                                xml.WriteElementString("merchantProductId", "SHIPBR");
                                xml.WriteElementString("merchantSKU", "SHIPBR");
                                xml.WriteElementString("vendorSKU", "SHIPBR");
                                xml.WriteElementString("unitPrice", (Pedido.HB_PEDIDO_LOGISTICAs.Sum(a => a.VALOR_FRETE) / Pedido.HB_PEDIDO_ITEMs.Count()).ToString().Replace(',', '.'));
                                xml.WriteElementString("unitCost", "BRL");
                                xml.WriteElementString("lineTax", "0");
                                xml.WriteElementString("lineSubTotal", (Pedido.HB_PEDIDO_LOGISTICAs.Sum(a => a.VALOR_FRETE) / Pedido.HB_PEDIDO_ITEMs.Count()).ToString().Replace(',', '.'));
                                xml.WriteEndElement();
                            }

                            xml.WriteEndElement();
                        }
                        xml.WriteEndElement();
                        xml.WriteEndElement();
                        xml.WriteEndDocument();
                    }
                }
                catch (Exception ex)
                {
                    Log.Write(String.Format($"{DateTime.Now} : \n ========== ERRO FATAL ========== \n {ex.ToString()}"));
                }
            }
        }
        public static void ProvenceXML(string Path, string NomeArquivo)
        {
            List<HB_PEDIDO_CAPA> PedidosProvence = (
                    from
                        a in _ctx.HbPedidoItem
                    join b in _ctx.MarcaProdutos on a.COD_PRODUTO equals b.COD_PRODUTO
                    where
                        a.HB_PEDIDO_CAPA.DATA_CANCELAMENTO == null
                        && a.XML_GERADO == false
                        && b.MARCAID == 2
                    select
                        a.HB_PEDIDO_CAPA
            ).Distinct().ToList();

            if (PedidosProvence.Any())
            {
                Log.Write(String.Format($"{DateTime.Now} : XML Provence - Gerando arquivo XML"));

                string Arquivo = String.Format($"{Path}{DateTime.Now.ToString("yyyyMMddhhmmss")}_{NomeArquivo}");
                try
                {
                    using (XmlWriter xml = XmlWriter.Create(Arquivo, new XmlWriterSettings { Indent = true, IndentChars = "  ", NewLineOnAttributes = false, OmitXmlDeclaration = true, Encoding = new UTF8Encoding(false) }))
                    {

                        xml.WriteStartDocument();
                        xml.WriteStartElement("ROOT");
                        xml.WriteStartElement("OrderMessageBatch");
                        xml.WriteAttributeString("batchNumber", "1");

                        foreach (var Pedido in PedidosProvence)
                        {
                            xml.WriteStartElement("hubOrder");
                            xml.WriteAttributeString("transactionID", (PedidosProvence.IndexOf(Pedido) + 1).ToString());
                            xml.WriteElementString("lineCount", Pedido.HB_PEDIDO_ITEMs.Count().ToString());
                            xml.WriteElementString("poNumber", Pedido.NUM_PEDIDO);
                            xml.WriteElementString("orderDate", Pedido.DATA_EMISSAO.ToString("yyyy-MM-dd"));
                            xml.WriteElementString("paymentMethod", "BOLETO");
                            xml.WriteElementString("tax", "0");
                            xml.WriteElementString("total", (Pedido.VALOR_TOTAL).ToString().Replace(',', '.')); //Não precisa abater o desconto. Já inclui o valor do frete
                            xml.WriteElementString("shippingCode", "Entrada REGULAR");

                            xml.WriteStartElement("shippingInfo");
                            xml.WriteElementString("name1", Pedido.HB_PEDIDO_ENDERECO.NOME_RECEBEDOR);
                            xml.WriteElementString("name2", "");
                            xml.WriteElementString("address1", Pedido.HB_PEDIDO_ENDERECO.ENDERECO + ", " + Pedido.HB_PEDIDO_ENDERECO.NUMERO);
                            xml.WriteElementString("address2", Pedido.HB_PEDIDO_ENDERECO.COMPLEMENTO);
                            xml.WriteElementString("address3", Pedido.HB_PEDIDO_ENDERECO.BAIRRO);
                            xml.WriteElementString("city", Pedido.HB_PEDIDO_ENDERECO.CIDADE);
                            xml.WriteElementString("state", Pedido.HB_PEDIDO_ENDERECO.ESTADO);
                            xml.WriteElementString("country", Pedido.HB_PEDIDO_ENDERECO.PAIS);
                            xml.WriteElementString("postalCode", Pedido.HB_PEDIDO_ENDERECO.CEP.Replace("-", ""));
                            xml.WriteElementString("phoneNumber", Pedido.HB_PEDIDO_CLIENTE.TELEFONE);
                            xml.WriteEndElement();

                            xml.WriteStartElement("billingInfo");
                            xml.WriteElementString("name1", Pedido.HB_PEDIDO_CLIENTE.NOME);
                            xml.WriteElementString("name2", Pedido.HB_PEDIDO_CLIENTE.SOBRENOME);
                            xml.WriteElementString("address1", Pedido.HB_PEDIDO_ENDERECO.ENDERECO + ", " + Pedido.HB_PEDIDO_ENDERECO.NUMERO);
                            xml.WriteElementString("address2", Pedido.HB_PEDIDO_ENDERECO.COMPLEMENTO);
                            xml.WriteElementString("address3", Pedido.HB_PEDIDO_ENDERECO.BAIRRO);
                            xml.WriteElementString("city", Pedido.HB_PEDIDO_ENDERECO.CIDADE);
                            xml.WriteElementString("state", Pedido.HB_PEDIDO_ENDERECO.ESTADO);
                            xml.WriteElementString("country", Pedido.HB_PEDIDO_ENDERECO.PAIS);
                            xml.WriteElementString("postalCode", Pedido.HB_PEDIDO_ENDERECO.CEP.Replace("-", ""));
                            xml.WriteElementString("phoneNumber", Pedido.HB_PEDIDO_CLIENTE.TELEFONE);
                            xml.WriteEndElement();

                            xml.WriteStartElement("customerInfo");
                            xml.WriteElementString("CPF", Pedido.HB_PEDIDO_CLIENTE.NUM_DOCUMENTO);
                            xml.WriteElementString("name1", Pedido.HB_PEDIDO_CLIENTE.NOME);
                            xml.WriteElementString("name2", Pedido.HB_PEDIDO_CLIENTE.SOBRENOME);
                            xml.WriteElementString("address1", Pedido.HB_PEDIDO_ENDERECO.ENDERECO + ", " + Pedido.HB_PEDIDO_ENDERECO.NUMERO);
                            xml.WriteElementString("address2", Pedido.HB_PEDIDO_ENDERECO.COMPLEMENTO);
                            xml.WriteElementString("address3", Pedido.HB_PEDIDO_ENDERECO.BAIRRO);
                            xml.WriteElementString("city", Pedido.HB_PEDIDO_ENDERECO.CIDADE);
                            xml.WriteElementString("state", Pedido.HB_PEDIDO_ENDERECO.ESTADO);
                            xml.WriteElementString("country", Pedido.HB_PEDIDO_ENDERECO.PAIS);
                            xml.WriteElementString("postalCode", Pedido.HB_PEDIDO_ENDERECO.CEP.Replace("-", ""));
                            xml.WriteElementString("phoneNumber", Pedido.HB_PEDIDO_CLIENTE.TELEFONE);
                            xml.WriteElementString("email", String.Format($"{Pedido.NUM_PEDIDO}@{Pedido.CANAL_SITE}.com.br"));
                            xml.WriteEndElement();
                            xml.WriteElementString("custOrderNumber", Pedido.NUM_PEDIDO);
                            xml.WriteElementString("custOrderDate", Pedido.DATA_EMISSAO.ToString("yyyy-MM-dd"));

                            int NUM_ITEM = 1;
                            foreach (var Item in Pedido.HB_PEDIDO_ITEMs.Where(a => (from marca in _ctx.MarcaProdutos where marca.MARCAID == 2 select marca.COD_PRODUTO).ToList().Contains(a.COD_PRODUTO)))
                            {
                                xml.WriteStartElement("lineItem");
                                xml.WriteElementString("lineItemId", NUM_ITEM.ToString());
                                xml.WriteElementString("orderLineNumber", NUM_ITEM.ToString());
                                xml.WriteElementString("qtyOrdered", Item.QUANTIDADE.ToString());
                                xml.WriteElementString("unitOfMeasure", "PECA");
                                xml.WriteElementString("description", "");
                                xml.WriteElementString("merchantProductId", Item.COD_PRODUTO);
                                xml.WriteElementString("merchantSKU", Item.COD_PRODUTO);
                                xml.WriteElementString("vendorSKU", Item.COD_PRODUTO);
                                xml.WriteElementString("unitPrice", (Item.VALOR_UNITARIO - Item.VALOR_DESCONTO).ToString().Replace(',', '.'));
                                xml.WriteElementString("unitCost", "BRL");
                                xml.WriteElementString("lineTax", "0");
                                xml.WriteElementString("lineSubTotal", Convert.ToString((Item.VALOR_UNITARIO * Item.QUANTIDADE) - Item.VALOR_DESCONTO).Replace(',', '.'));
                                xml.WriteEndElement();

                                Item.XML_GERADO = true;
                                _ctx.Entry(Item).State = EntityState.Modified;
                                _ctx.SaveChanges();

                                HB_PEDIDO_XML HbPedidoXml = new HB_PEDIDO_XML
                                {
                                    ITEMID = Item.ID,
                                    NOME_ARQUIVO = Arquivo,
                                    CADASTRADO_POR = "INTEGRACAO",
                                    DATA_CADASTRO = DateTime.Now,
                                    ALTERADO_POR = "INTEGRACAO",
                                    DATA_ALTERACAO = DateTime.Now
                                };

                                _ctx.Set<HB_PEDIDO_XML>().Add(HbPedidoXml);
                                _ctx.SaveChanges();

                                NUM_ITEM++;
                            }

                            if (Pedido.HB_PEDIDO_LOGISTICAs.Any() && Pedido.HB_PEDIDO_LOGISTICAs.Sum(a => a.VALOR_FRETE) > 0)
                            {
                                xml.WriteStartElement("lineItem");
                                xml.WriteElementString("lineItemId", NUM_ITEM.ToString());
                                xml.WriteElementString("orderLineNumber", NUM_ITEM.ToString());
                                xml.WriteElementString("qtyOrdered", "1");
                                xml.WriteElementString("unitOfMeasure", "PECA");
                                xml.WriteElementString("description", "FRETE");
                                xml.WriteElementString("merchantProductId", "SHIPBR");
                                xml.WriteElementString("merchantSKU", "SHIPBR");
                                xml.WriteElementString("vendorSKU", "SHIPBR");
                                xml.WriteElementString("unitPrice", (Pedido.HB_PEDIDO_LOGISTICAs.Sum(a => a.VALOR_FRETE) / Pedido.HB_PEDIDO_ITEMs.Count()).ToString().Replace(',', '.'));
                                xml.WriteElementString("unitCost", "BRL");
                                xml.WriteElementString("lineTax", "0");
                                xml.WriteElementString("lineSubTotal", (Pedido.HB_PEDIDO_LOGISTICAs.Sum(a => a.VALOR_FRETE) / Pedido.HB_PEDIDO_ITEMs.Count()).ToString().Replace(',', '.'));
                                xml.WriteEndElement();
                            }
                            xml.WriteEndElement();
                        }
                        xml.WriteEndElement();
                        xml.WriteEndElement();
                        xml.WriteEndDocument();
                    }

                }
                catch (Exception ex)
                {
                    Log.Write(String.Format($"{DateTime.Now} : \n ========== ERRO FATAL ========== \n {ex.ToString()}"));
                }
            }
        }
        public static void ProvenceImportarDadosCSV(string Path, string NomeArquivo)
        {
            List<OCMSQuery> ListaOcmsQuery = new List<OCMSQuery>();
            if (File.Exists(Path + NomeArquivo))
            {
                foreach (string line in File.ReadLines(Path + NomeArquivo, Encoding.GetEncoding("iso-8859-1")).Skip(1))
                {
                    try 
                    {
                        OCMSQuery _ocmsQuery = new OCMSQuery
                        {
                            Order               = line.Split(';')[0],
                            Customer            = line.Split(';')[1],
                            Email               = line.Split(';')[2],
                            FirstName           = line.Split(';')[3],
                            LastName            = line.Split(';')[4],
                            NbLine              = line.Split(';')[5],
                            OrderTotal          = line.Split(';')[6],
                            ShippingFee         = line.Split(';')[7],
                            Tax                 = line.Split(';')[8],
                            GlobalTotal         = line.Split(';')[9],
                            Status              = line.Split(';')[10],
                            Comment             = line.Split(';')[11],
                            DateCreated         = Convert.ToDateTime(line.Split(';')[12]),
                            Info                = line.Split(';')[13],
                            Tracking            = line.Split(';')[14],
                            ShippingMethod      = line.Split(';')[15],
                            PaymentMethod       = line.Split(';')[16],
                            AuthNumber          = line.Split(';')[17],
                            TransactionID       = line.Split(';')[18],
                            CardType            = line.Split(';')[19],
                            LastModifiedBy      = line.Split(';')[20],
                            DateModified        = Convert.ToDateTime(line.Split(';')[21]),
                            DiscountTotal       = line.Split(';')[22],
                            ShippingFirstName   = line.Split(';')[23],
                            ShippingLastName    = line.Split(';')[24],
                            ShippingPhone       = line.Split(';')[25],
                            ShippingZip         = line.Split(';')[26],
                            ShippingCity        = line.Split(';')[27],
                            ShippingState       = line.Split(';')[28],
                            CustomerOptional1   = line.Split(';')[29],
                            CustomerOptional2   = line.Split(';')[30],
                            CustomerOptional3   = line.Split(';')[31],
                            CustomerOptional4   = line.Split(';')[32],
                            CustomerOptional5   = line.Split(';')[33],
                            CustomerAddress     = line.Split(';')[34],
                            CustomerAddress2    = line.Split(';')[35],
                            CustomerAddress3    = line.Split(';')[36],
                            CustomerAddress4    = line.Split(';')[37],
                            CustomerAddress5    = line.Split(';')[38],
                            CustomerAddress6    = line.Split(';')[39],
                            CustomerAddress7    = line.Split(';')[40],
                            CustomerAddress8    = line.Split(';')[41],
                            CustomerAddress9    = line.Split(';')[42],
                            ShippingAddress     = line.Split(';')[43],
                            ShippingAddress2    = line.Split(';')[44],
                            ShippingAddress3    = line.Split(';')[45],
                            ShippingAddress4    = line.Split(';')[46],
                            ShippingAddress5    = line.Split(';')[47],
                            ShippingAddress6    = line.Split(';')[48],
                            ShippingAddress7    = line.Split(';')[49],
                            ShippingAddress8    = line.Split(';')[50],
                            ShippingAddress9    = line.Split(';')[51],
                            Source              = line.Split(';')[52],
                            GiftMessage         = line.Split(';')[53],
                            GiftInstructions    = line.Split(';')[54]
                        };
                        ListaOcmsQuery.Add(_ocmsQuery);
                    }
                    catch(Exception ex)
                    {
                        continue;
                    }

                }

                List<HB_INTEGRACAO_OCMS> lista = new List<HB_INTEGRACAO_OCMS>();
                using (HuB2BDataContext _ctx = new HuB2BDataContext())
                {

                    lista = (
                                from tbl in ListaOcmsQuery.Select(a => new { a.Order, a.Email, a.Status, a.DateCreated, a.DateModified }).Distinct()
                                select new HB_INTEGRACAO_OCMS
                                {
                                    OCMSID = tbl.Order,
                                    NUM_PEDIDO = tbl.Email.Substring(0, tbl.Email.IndexOf("@")),
                                    EMAILIDENT = tbl.Email,
                                    MARCA = 2, //PROVENCE 
                                    STATUS = tbl.Status,
                                    CREATIONDATE = Convert.ToDateTime(tbl.DateCreated),
                                    MODIFIEDDATE = Convert.ToDateTime(tbl.DateModified)
                                }
                            ).ToList();

                    foreach (HB_INTEGRACAO_OCMS linha in lista)
                    {
                        try
                        {
                            var ret = _ctx.HbIntegracaoOCMS
                                .AsNoTracking()
                                .FirstOrDefault(a => a.OCMSID == linha.OCMSID && a.NUM_PEDIDO == linha.NUM_PEDIDO);

          
                            if (ret == null)
                            {

                                linha.DATA_CADASTRO = DateTime.Now;
                                linha.CADASTRADO_POR = "INTEGRACAO";

                                linha.DATA_ALTERACAO = null;
                                linha.ALTERADO_POR = null;

                                _ctx.Set<HB_INTEGRACAO_OCMS>().Add(linha);
                                _ctx.SaveChanges();
                            }
                            else
                            {
                                linha.DATA_CADASTRO = ret.DATA_CADASTRO;
                                linha.CADASTRADO_POR = ret.CADASTRADO_POR;

                                linha.DATA_ALTERACAO = DateTime.Now;
                                linha.ALTERADO_POR = "INTEGRAÇÃO";

                                _ctx.Entry<HB_INTEGRACAO_OCMS>(linha).State = EntityState.Modified;
                                _ctx.SaveChanges();
                            }
                            
                        }
                        catch(Exception ex)
                        {
                            continue;
                        }                      
                        
                    }
   
                }
            }           

        }
        public static void AuBresilImportarDadosCSV(string Path, string NomeArquivo)
        {
            List<OCMSQuery> ListaOcmsQuery = new List<OCMSQuery>();
            if (File.Exists(Path + NomeArquivo))
            {
                foreach (string line in File.ReadLines(Path + NomeArquivo, Encoding.GetEncoding("iso-8859-1")).Skip(1))
                {
                    try
                    {
                        OCMSQuery _ocmsQuery = new OCMSQuery
                        {
                            Order = line.Split(';')[0],
                            Customer = line.Split(';')[1],
                            Email = line.Split(';')[2],
                            FirstName = line.Split(';')[3],
                            LastName = line.Split(';')[4],
                            NbLine = line.Split(';')[5],
                            OrderTotal = line.Split(';')[6],
                            ShippingFee = line.Split(';')[7],
                            Tax = line.Split(';')[8],
                            GlobalTotal = line.Split(';')[9],
                            Status = line.Split(';')[10],
                            Comment = line.Split(';')[11],
                            DateCreated = Convert.ToDateTime(line.Split(';')[12]),
                            Info = line.Split(';')[13],
                            Tracking = line.Split(';')[14],
                            ShippingMethod = line.Split(';')[15],
                            PaymentMethod = line.Split(';')[16],
                            AuthNumber = line.Split(';')[17],
                            TransactionID = line.Split(';')[18],
                            CardType = line.Split(';')[19],
                            LastModifiedBy = line.Split(';')[20],
                            DateModified =Convert.ToDateTime(line.Split(';')[21]),
                            DiscountTotal = line.Split(';')[22],
                            ShippingFirstName = line.Split(';')[23],
                            ShippingLastName = line.Split(';')[24],
                            ShippingPhone = line.Split(';')[25],
                            ShippingZip = line.Split(';')[26],
                            ShippingCity = line.Split(';')[27],
                            ShippingState = line.Split(';')[28],
                            CustomerOptional1 = line.Split(';')[29],
                            CustomerOptional2 = line.Split(';')[30],
                            CustomerOptional3 = line.Split(';')[31],
                            CustomerOptional4 = line.Split(';')[32],
                            CustomerOptional5 = line.Split(';')[33],
                            CustomerAddress = line.Split(';')[34],
                            CustomerAddress2 = line.Split(';')[35],
                            CustomerAddress3 = line.Split(';')[36],
                            CustomerAddress4 = line.Split(';')[37],
                            CustomerAddress5 = line.Split(';')[38],
                            CustomerAddress6 = line.Split(';')[39],
                            CustomerAddress7 = line.Split(';')[40],
                            CustomerAddress8 = line.Split(';')[41],
                            CustomerAddress9 = line.Split(';')[42],
                            ShippingAddress = line.Split(';')[43],
                            ShippingAddress2 = line.Split(';')[44],
                            ShippingAddress3 = line.Split(';')[45],
                            ShippingAddress4 = line.Split(';')[46],
                            ShippingAddress5 = line.Split(';')[47],
                            ShippingAddress6 = line.Split(';')[48],
                            ShippingAddress7 = line.Split(';')[49],
                            ShippingAddress8 = line.Split(';')[50],
                            ShippingAddress9 = line.Split(';')[51],
                            Source = line.Split(';')[52],
                            GiftMessage = line.Split(';')[53],
                            GiftInstructions = line.Split(';')[54]
                        };
                        ListaOcmsQuery.Add(_ocmsQuery);
                    }
                    catch (Exception ex)
                    {
                        continue;
                    }

                }

                List<HB_INTEGRACAO_OCMS> lista = new List<HB_INTEGRACAO_OCMS>();
                using (HuB2BDataContext _ctx = new HuB2BDataContext())
                {

                    lista = (
                                from tbl in ListaOcmsQuery.Select(a => new { a.Order, a.Email, a.Status, a.DateCreated, a.DateModified }).Distinct()
                                select new HB_INTEGRACAO_OCMS
                                {
                                    OCMSID = tbl.Order,
                                    NUM_PEDIDO = tbl.Email.Substring(0, tbl.Email.IndexOf("@")),
                                    EMAILIDENT = tbl.Email,
                                    MARCA = 1, //AU BRESIL 
                                    STATUS = tbl.Status,
                                    CREATIONDATE = Convert.ToDateTime(tbl.DateCreated),
                                    MODIFIEDDATE = Convert.ToDateTime(tbl.DateModified)
                               }
                            ).ToList();

                    foreach (HB_INTEGRACAO_OCMS linha in lista)
                    {
                        try
                        {
                            var ret = _ctx.HbIntegracaoOCMS
                                .AsNoTracking()
                                .FirstOrDefault(a => a.OCMSID == linha.OCMSID && a.NUM_PEDIDO == linha.NUM_PEDIDO);


                            if (ret == null)
                            {

                                linha.DATA_CADASTRO = DateTime.Now;
                                linha.CADASTRADO_POR = "INTEGRACAO";

                                linha.DATA_ALTERACAO = null;
                                linha.ALTERADO_POR = null;

                                _ctx.Set<HB_INTEGRACAO_OCMS>().Add(linha);
                                _ctx.SaveChanges();
                            }
                            else
                            {
                                linha.DATA_CADASTRO = ret.DATA_CADASTRO;
                                linha.CADASTRADO_POR = ret.CADASTRADO_POR;

                                linha.DATA_ALTERACAO = DateTime.Now;
                                linha.ALTERADO_POR = "INTEGRAÇÃO";

                                _ctx.Entry(linha).State = EntityState.Modified;
                                _ctx.SaveChanges();
                            }

                        }
                        catch (Exception ex)
                        {
                            continue;
                        }

                    }

                }
            }

        }
    }
}
