﻿using HuB2B.WinService.Data;
using HuB2B.WinService.WebApi;
using System;
using System.Threading.Tasks;
using System.Timers;

namespace HuB2B.WinService.Executores
{
    public class TimerHuB2B
    {
        static Timer timer;
        static readonly HuB2BDataContext _ctx = new HuB2BDataContext();

        public static void TimeScheduler()
        {
            try
            {
                Log.Write(strMensagem: String.Format($"{DateTime.Now} : Realizando agendamento..."), Tipo: 0);

                DateTime ScheduledTime = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 3, 30, 0, 0);
                if (DateTime.Now > ScheduledTime)
                {
                    ScheduledTime = DateTime.Now.AddMinutes(60);
                }
            
                Log.Write(String.Format($"{DateTime.Now} : Tarefa agendada para {ScheduledTime}"));

                double TickTime = (double)(ScheduledTime - DateTime.Now).TotalMilliseconds;
                timer = new Timer(TickTime);
                timer.Elapsed += new ElapsedEventHandler(TimeElapsed);
                timer.Start();
            }
            catch (Exception ex)
            {
                Log.Write(String.Format($"{DateTime.Now} : \n ========== ERRO FATAL ========== \n {ex.ToString()}"));
                Email.Enviar("am03svc-cegid@loccitane.com.br", "diego.bueno@loccitane.com", ex.ToString());
            }
        }

        private static void TimeElapsed(object sender, ElapsedEventArgs e)
        {
            try
            {
                timer.Stop();
                Log.Write(String.Format($"{DateTime.Now} : Iniciando a tarefa..."));

                string ChaveAuth = _ctx.HbParametros.Find("CHAVE_AUTH").VALOR;
                string IdTenant = _ctx.HbParametros.Find("ID_TENANT").VALOR;
                Uri baseAddress = new Uri(_ctx.HbParametros.Find("BASE_ADDRESS").VALOR);

                Log.Write(String.Format($"{DateTime.Now} : Enviando dados WebApi..."));
            
                Task.Run(async () =>
                {
                    await apiPedidos.PedidosID(baseAddress, IdTenant, ChaveAuth);
                    await apiPedidos.Pedidos(baseAddress, IdTenant, ChaveAuth);
                    await apiPedidos.ConfirmaDownload(baseAddress, IdTenant, ChaveAuth);
                    await apiPedidos.AtualizarFaturamento(baseAddress, IdTenant, ChaveAuth);
                    await apiEstoque.AtualizarEstoque(baseAddress, IdTenant, ChaveAuth);
                }).Wait();

                OCMS.AuBresilXML(_ctx.HbParametros.Find("FTP_AUBRESIL").VALOR, @"ORDER_Multiplus-OAB.xml");
                OCMS.ProvenceXML(_ctx.HbParametros.Find("FTP_PROVENCE").VALOR, @"ORDER_Multiplus-OEP.xml");

                SFTP.FileUpload(_ctx.HbParametros.Find("FTP_AUBRESIL").VALOR, @"10.33.17.4", 22, "am03svc-ftpecomm", "ecoFmmeTrceP", @"\\Multiplus\Multiplus_Orders_OAB\");
                SFTP.FileUpload(_ctx.HbParametros.Find("FTP_PROVENCE").VALOR, @"10.33.17.4", 22, "am03svc-ftpecomm", "ecoFmmeTrceP", @"\\Multiplus\Multiplus_Orders_OEP\");

                if (DateTime.Now.DayOfWeek == DayOfWeek.Friday && DateTime.Now.Hour > 9 && DateTime.Now.Hour < 10)
                {
                    Relatorio.Enviar();
                }
            }
            catch(Exception ex)
            {
                Log.Write(String.Format($"{DateTime.Now} : \n ========== ERRO FATAL ========== \n {ex.ToString()}"));
                Email.Enviar("am03svc-cegid@loccitane.com.br", "wagner.damasio@loccitane.com", ex.ToString());
            }

            Log.Write(String.Format($"{DateTime.Now} : Fim da tarefa..."));
            TimeScheduler();
        }
    }
}
