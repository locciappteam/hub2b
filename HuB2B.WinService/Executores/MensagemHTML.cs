﻿using HuB2B.WinService.Entidades.Pedidos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HuB2B.WinService.Executores
{
    public static class MensagemHTML
    {
        public static string Formatar(List<HB_PEDIDO_CAPA> HbPedidoCapa)
        {
            string Mensagem = "<style>table.blueTable{border:1px solid #1C6EA4;background-color:#EEEEEE;width:100%;text-align:left;border-collapse:collapse;}table.blueTable td, table.blueTable th{border:1px solid #AAAAAA;padding:3px 2px;}table.blueTable tbody td{font-size:13px;}table.blueTable tr:nth-child(even){background:#D0E4F5;}table.blueTable thead{background:#1C6EA4;background:-moz-linear-gradient(top, #5592bb 0%, #327cad 66%, #1C6EA4 100%);background:-webkit-linear-gradient(top, #5592bb 0%, #327cad 66%, #1C6EA4 100%);background:linear-gradient(to bottom, #5592bb 0%, #327cad 66%, #1C6EA4 100%);border-bottom:2px solid #444444;}table.blueTable thead th{font-size:15px;font-weight:bold;color:#FFFFFF;border-left:2px solid #D0E4F5;}table.blueTable thead th:first-child{border-left:none;}table.blueTable tfoot{font-size:14px;font-weight:bold;color:#FFFFFF;background:#D0E4F5;background:-moz-linear-gradient(top, #dcebf7 0%, #d4e6f6 66%, #D0E4F5 100%);background:-webkit-linear-gradient(top, #dcebf7 0%, #d4e6f6 66%, #D0E4F5 100%);background:linear-gradient(to bottom, #dcebf7 0%, #d4e6f6 66%, #D0E4F5 100%);border-top:2px solid #444444;}table.blueTable tfoot td{font-size:14px;}table.blueTable tfoot .links{text-align:right;}table.blueTable tfoot .links a{display:inline-block;background:#1C6EA4;color:#FFFFFF;padding:2px 8px;border-radius:5px;}</style>" +
            "<table class='blueTable'>" +
                "<thead>" +
                    "<tr>" +
                        "<th>PEDIDO</th>" +
                        "<th>RAZÃO SOCIAL</th>" +
                        "<th>DATA COMPRA</th>" +
                        "<th>SITE</th>" +
                        "<th>VALOR TOTAL</th>" +
                        "<th>DESCONTO</th>" +
                        "<th>STATUS</th>" +
                    "</tr>" +
                "</thead>" +
                "<tbody>";
                    foreach (HB_PEDIDO_CAPA Pedido in HbPedidoCapa)
                    {
                        Mensagem +=
                        "<tr>" +
                            String.Format($"<td>{Pedido.NUM_PEDIDO}</td>") +
                            String.Format($"<td>{Pedido.HB_PEDIDO_CLIENTE.RAZAO_SOCIAL}</td>") +
                            String.Format($"<td>{Pedido.DATA_EMISSAO}</td>") +
                            String.Format($"<td>{Pedido.CANAL_SITE}</td>") +
                            String.Format($"<td>{Pedido.VALOR_TOTAL}</td>") +
                            String.Format($"<td>{Pedido.DESCONTO_TOTAL}</td>") +
                            String.Format($"<td>{Pedido.STATUS_PEDIDO}</td>") +
                        "</tr>";
                    } Mensagem +=
                "</tbody>" +
            "</table>";

            return Mensagem;
        }
    }
}
