﻿using HuB2B.WinService.Data;
using HuB2B.WinService.Entidades.Pedidos;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace HuB2B.WinService.Executores
{
    public static class Relatorio
    {
        public static void Enviar()
        {
            using (HuB2BDataContext _ctx = new HuB2BDataContext())
            {
                List<HB_PEDIDO_CAPA> HbPedidoCapa = (
                    from
                        capa in _ctx.HbPedidoCapa
                    join
                        item in _ctx.HbPedidoItem on capa.ID equals item.PEDIDOID
                    from
                        faturado in _ctx.HbPedidoFaturado.Where(a => a.ID == item.ID).DefaultIfEmpty()
                    where
                        capa.DATA_CANCELAMENTO == null
                        && faturado.ID == null
                        //&& DbFunctions.DiffDays(capa.DATA_EMISSAO, DateTime.Now).Value >= 1
                        //&& DateTime.Now.Hour == 9 // Enviar somente 1 vez ao dia
                    select
                        capa
                ).Distinct().ToList();

                if (HbPedidoCapa.Any())
                {
                    string Mensagem = MensagemHTML.Formatar(HbPedidoCapa);
                    Email.Enviar("am03svc-cegid@loccitane.com.br", "wagner.damasio@loccitane.com", Mensagem, "HuB2B - Pedidos não faturado", true);
                    //Email.Enviar("am03svc-cegid@loccitane.com.br", "isadora.nascimento@loccitane.com, tassia.sofia@loccitane.com, wagner.damasio@loccitane.com", Mensagem, "HuB2B - Pedidos não faturado", true);
                }
            }
        }
    }
}
