﻿using HuB2B.WinService.Entidades.Pedidos;
using HuB2B.WinService.Entidades.Universal;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace HuB2B.WinService.Entidades.HUB2B
{
    public class HB_CANAL_VENDAS
    {
        public HB_CANAL_VENDAS()
        {
            DATA_ALTERACAO = DateTime.Now;
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }
        public int COD_CANAL { get; set; }
        public string CANAL { get; set; }

        public int? MARCALOCCITANEID { get; set; }
        [ForeignKey("MARCALOCCITANEID")]
        public virtual MARCA_LOCCITANE MARCA_LOCCITANE { get; set; }

        public bool ATIVADO { get; set; }
        public string CADASTRADO_POR { get; set; }
        public DateTime DATA_CADASTRO { get; set; }
        public string ALTERADO_POR { get; set; }
        public DateTime DATA_ALTERACAO { get; set; }

        public virtual ICollection<HB_PEDIDO_CAPA> HB_PEDIDO_CAPAs { get; set; }
        public virtual ICollection<HB_PEDIDO_ID> HB_PEDIDO_IDs { get; set; }
    }
}
