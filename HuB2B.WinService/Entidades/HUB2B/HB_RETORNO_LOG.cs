﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace HuB2B.WinService.Entidades.HUB2B
{
    public class HB_RETORNO_LOG
    {
        public HB_RETORNO_LOG()
        {
            DATA_ALTERACAO = DateTime.Now;
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }
        public string COD_RETORNO { get; set; }
        public string DESC_RETORNO { get; set; }
        public string INTEGRACAO { get; set; }
        public string CADASTRADO_POR { get; set; }
        public DateTime DATA_CADASTRO { get; set; }
        public string ALTERADO_POR { get; set; }
        public DateTime DATA_ALTERACAO { get; set; }
    }
}
