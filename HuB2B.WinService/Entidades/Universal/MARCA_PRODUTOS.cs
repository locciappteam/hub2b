﻿using HuB2B.WinService.Entidades.Pedidos;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HuB2B.WinService.Entidades.Universal
{
    public class MARCA_PRODUTOS
    {
        public MARCA_PRODUTOS()
        {
            DATA_ALTERACAO = DateTime.Now;
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }

        public string COD_PRODUTO { get; set; }
        public string DESC_PRODUTO { get; set; }

        [ForeignKey("MARCA_LOCCITANE")]
        public int MARCAID { get; set; }

        public string CADASTRADO_POR { get; set; }
        public DateTime DATA_CADASTRO { get; set; }
        public string ALTERADO_POR { get; set; }
        public DateTime DATA_ALTERACAO { get; set; }

        public virtual MARCA_LOCCITANE MARCA_LOCCITANE { get; set; }
    }
}
