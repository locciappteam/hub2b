﻿using HuB2B.WinService.Entidades.HUB2B;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace HuB2B.WinService.Entidades.Universal
{
    public class MARCA_LOCCITANE
    {
        public MARCA_LOCCITANE()
        {
            DATA_ALTERACAO = DateTime.Now;
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }
        public string MARCA { get; set; }
        public bool ATIVO { get; set; }
        public string CADASTRADO_POR { get; set; }
        public DateTime DATA_CADASTRO { get; set; }
        public string ALTERADO_POR { get; set; }
        public DateTime DATA_ALTERACAO { get; set; }

        public virtual ICollection<HB_CANAL_VENDAS> HB_CANAL_VENDASs { get; set; }
        public virtual ICollection<MARCA_PRODUTOS> MARCA_PRODUTOSs { get; set; }
    }
}
