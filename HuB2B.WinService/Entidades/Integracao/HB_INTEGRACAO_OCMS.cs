﻿using System;
using System.ComponentModel.DataAnnotations;

namespace HuB2B.WinService.Entidades.Integracao
{
    public class HB_INTEGRACAO_OCMS
    {

        public string OCMSID { get; set; }
        public string NUM_PEDIDO { get; set; }
        public int MARCA { get; set; }
        public string EMAILIDENT { get; set; }
        public string STATUS { get; set; }

        public DateTime? CREATIONDATE { get; set; }
        public DateTime? MODIFIEDDATE { get; set; }

        public DateTime? DATA_CADASTRO { get; set; }
        public string CADASTRADO_POR { get; set; }
        public string ALTERADO_POR { get; set; }
        public DateTime? DATA_ALTERACAO { get; set; }

    }
}
