﻿using HuB2B.WinService.Entidades.Universal;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace HuB2B.WinService.Entidades.Estoque
{
    public class HB_ESTOQUE_CONTROLE
    {
        public HB_ESTOQUE_CONTROLE()
        {
            DATA_ALTERACAO = DateTime.Now;
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }

        public int ESTABELECIMENTOID { get; set; }
        [ForeignKey("ESTABELECIMENTOID")]
        public virtual ESTABELECIMENTO ESTABELECIMENTO { get; set; }

        public int CROSSDOCKING { get; set; }
        public string CADASTRADO_POR { get; set; }
        public DateTime DATA_CADASTRO { get; set; }
        public string ALTERADO_POR { get; set; }
        public DateTime DATA_ALTERACAO { get; set; }

        public virtual ICollection<HB_ESTOQUE_ITEM> HB_ESTOQUE_ITEMs { get; set; }
        public virtual ICollection<HB_ESTOQUE_RETORNO> HB_ESTOQUE_RETORNOs { get; set; }
    }
}
