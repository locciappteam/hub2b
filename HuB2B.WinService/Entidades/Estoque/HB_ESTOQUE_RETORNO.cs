﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace HuB2B.WinService.Entidades.Estoque
{
    public class HB_ESTOQUE_RETORNO
    {
        public HB_ESTOQUE_RETORNO()
        {
            DATA_ALTERACAO = DateTime.Now;
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }

        public int ESTOQUEID { get; set; }
        [ForeignKey("ESTOQUEID")]
        public virtual HB_ESTOQUE_CONTROLE HB_ESTOQUE_CONTROLE { get; set; }

        public string COD_RETORNO { get; set; }
        public string DESC_ERRO { get; set; }
        public string DESC_DATA { get; set; }

        public string CADASTRADO_POR { get; set; }
        public DateTime DATA_CADASTRO { get; set; }
        public string ALTERADO_POR { get; set; }
        public DateTime DATA_ALTERACAO { get; set; }
    }
}
