﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace HuB2B.WinService.Entidades.Estoque
{
    public class HB_ESTOQUE_ITEM
    {
        public HB_ESTOQUE_ITEM()
        {
            DATA_ALTERACAO = DateTime.Now;
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }

        public int ESTOQUEID { get; set; }
        [ForeignKey("ESTOQUEID")]
        public virtual HB_ESTOQUE_CONTROLE HB_ESTOQUE_CONTROLE { get; set; }

        public string COD_PRODUTO { get; set; }
        public int? QTDE { get; set; }
        public string CADASTRADO_POR { get; set; }
        public DateTime DATA_CADASTRO { get; set; }
        public string ALTERADO_POR { get; set; }
        public DateTime DATA_ALTERACAO { get; set; }
    }
}
