﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace HuB2B.WinService.Entidades.Pedidos
{
    public class HB_PEDIDO_RASTREAMENTO
    {
        public HB_PEDIDO_RASTREAMENTO()
        {
            DATA_ALTERACAO = DateTime.Now;
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }

        public int PEDIDOID { get; set; }
        [ForeignKey("PEDIDOID")]
        public virtual HB_PEDIDO_CAPA HB_PEDIDO_CAPA { get; set; }

        public string NUMERO_NFE { get; set; }
        public string SERIE_NFE { get; set; }
        public string CHAVE_NFE { get; set; }
        public DateTime DATA_EMISSAO { get; set; }
        public string CODIGO_RASTREIO { get; set; }
        public string URL_RASTREIO { get; set; }
        public string CADASTRADO_POR { get; set; }
        public DateTime DATA_CADASTRO { get; set; }
        public string ALTERADO_POR { get; set; }
        public DateTime DATA_ALTERACAO { get; set; }
    }
}
