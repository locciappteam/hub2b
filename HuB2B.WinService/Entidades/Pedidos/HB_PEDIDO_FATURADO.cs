﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HuB2B.WinService.Entidades.Pedidos
{
    public class HB_PEDIDO_FATURADO
    {
        public HB_PEDIDO_FATURADO()
        {
            DATA_ALTERACAO = DateTime.Now;
        }

        [Key, ForeignKey("HB_PEDIDO_ITEM")]
        public int? ID { get; set; }

        public DateTime DATA_RECEBIMENTO { get; set; }
        public string COD_RECEPTOR { get; set; }
        public string DESC_RETORNO { get; set; }
        public string CADASTRADO_POR { get; set; }
        public DateTime DATA_CADASTRO { get; set; }
        public string ALTERADO_POR { get; set; }
        public DateTime DATA_ALTERACAO { get; set; }

        public virtual HB_PEDIDO_ITEM HB_PEDIDO_ITEM { get; set; }
    }
}
