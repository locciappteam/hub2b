﻿using HuB2B.WinService.Entidades.Universal;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace HuB2B.WinService.Entidades.Pedidos
{
    public class HB_PEDIDO_ITEM
    {
        public HB_PEDIDO_ITEM()
        {
            DATA_ALTERACAO = DateTime.Now;
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]        
        public int ID { get; set; }

        [ForeignKey("HB_PEDIDO_CAPA")]
        public int PEDIDOID { get; set; }
        
        public string COD_PRODUTO { get; set; }
        public int QUANTIDADE { get; set; }
        public decimal VALOR_UNITARIO { get; set; }
        public decimal VALOR_DESCONTO { get; set; }
        public bool? XML_GERADO { get; set; }
        public string CADASTRADO_POR { get; set; }
        public DateTime DATA_CADASTRO { get; set; }
        public string ALTERADO_POR { get; set; }
        public DateTime DATA_ALTERACAO { get; set; }

        public virtual HB_PEDIDO_CAPA HB_PEDIDO_CAPA { get; set; }
        public virtual HB_PEDIDO_FATURADO HB_PEDIDO_FATURADO { get; set; }
        public virtual ICollection<HB_PEDIDO_XML> HB_PEDIDO_XMLs { get; set; }
    }
}
