﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace HuB2B.WinService.Entidades.Pedidos
{
    public class HB_PEDIDO_PAGAMENTO
    {
        public HB_PEDIDO_PAGAMENTO()
        {
            DATA_ALTERACAO = DateTime.Now;
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }

        public int PEDIDOID { get; set; }
        [ForeignKey("PEDIDOID")]
        public virtual HB_PEDIDO_CAPA HB_PEDIDO_CAPA { get; set; }

        public int NUMERO_PARCELAS { get; set; }
        public string METODO_PAGAMENTO { get; set; }
        public decimal VALOR_PAGAMENTO { get; set; }
        public string CADASTRADO_POR { get; set; }
        public DateTime DATA_CADASTRO { get; set; }
        public string ALTERADO_POR { get; set; }
        public DateTime DATA_ALTERACAO { get; set; }
    }
}
