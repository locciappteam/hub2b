﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace HuB2B.WinService.Entidades.Pedidos
{
    public class HB_PEDIDO_ENDERECO
    {
        public HB_PEDIDO_ENDERECO()
        {
            DATA_ALTERACAO = DateTime.Now;
        }

        [Key, ForeignKey("HB_PEDIDO_CAPA")]
        public int ID { get; set; }

        public string ENDERECO { get; set; }
        public string NUMERO { get; set; }
        public string CEP { get; set; }
        public string CIDADE { get; set; }
        public string ESTADO { get; set; }
        public string BAIRRO { get; set; }
        public string COMPLEMENTO { get; set; }
        public int? CODIGO_IBGE { get; set; }
        public string PAIS { get; set; }
        public string NOME_RECEBEDOR { get; set; }
        public string CADASTRADO_POR { get; set; }
        public DateTime DATA_CADASTRO { get; set; }
        public string ALTERADO_POR { get; set; }
        public DateTime DATA_ALTERACAO { get; set; }

        public virtual HB_PEDIDO_CAPA HB_PEDIDO_CAPA { get; set; }
    }
}
