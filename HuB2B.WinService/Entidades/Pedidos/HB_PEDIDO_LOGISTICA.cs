﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace HuB2B.WinService.Entidades.Pedidos
{
    public class HB_PEDIDO_LOGISTICA
    {
        public HB_PEDIDO_LOGISTICA()
        {
            DATA_ALTERACAO = DateTime.Now;
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }

        [ForeignKey("HB_PEDIDO_CAPA")]
        public int PEDIDOID { get; set; }

        public string DIAS_EST_ENTREGA { get; set; }
        public DateTime DATA_EST_ENTREGA { get; set; }
        public decimal VALOR_FRETE { get; set; }
        public string COD_TRANSPORTADORA { get; set; }
        public string NOME_TRANSPORTADORA { get; set; }
        public int QTDE_ENVIADA { get; set; }
        public int ARMAZEMID { get; set; }
        public string CADASTRADO_POR { get; set; }
        public DateTime DATA_CADASTRO { get; set; }
        public string ALTERADO_POR { get; set; }
        public DateTime DATA_ALTERACAO { get; set; }

        public virtual HB_PEDIDO_CAPA HB_PEDIDO_CAPA { get; set; }
    }
}
