﻿using HuB2B.WinService.Entidades.HUB2B;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace HuB2B.WinService.Entidades.Pedidos
{
    public class HB_PEDIDO_ID
    {
        public HB_PEDIDO_ID()
        {
            DATA_ALTERACAO = DateTime.Now;
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }
        public string NUM_PEDIDO { get; set; }
        public string NUM_MARKETPLACE { get; set; }
        public string CANAL_VENDA_PREFIXO { get; set; }

        public int CANALVENDAID { get; set; }
        [ForeignKey("CANALVENDAID")]
        public virtual HB_CANAL_VENDAS HB_CANAL_VENDAS { get; set; }

        public string CADASTRADO_POR { get; set; }
        public DateTime DATA_CADASTRO { get; set; }
        public string ALTERADO_POR { get; set; }
        public DateTime DATA_ALTERACAO { get; set; }
    }
}
