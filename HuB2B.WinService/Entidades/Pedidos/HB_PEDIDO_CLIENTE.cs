﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace HuB2B.WinService.Entidades.Pedidos
{
    public class HB_PEDIDO_CLIENTE
    {
        public HB_PEDIDO_CLIENTE()
        {
            DATA_ALTERACAO = DateTime.Now;
        }

        [Key, ForeignKey("HB_PEDIDO_CAPA")]
        public int ID { get; set; }

        public string NOME { get; set; }
        public string SOBRENOME { get; set; }
        public string EMAIL { get; set; }
        public string TIPO_DOCUMENTO { get; set; }
        public string NUM_DOCUMENTO { get; set; }
        public string TELEFONE { get; set; }
        public bool PESSOA_JURIDICA { get; set; }
        public string RAZAO_SOCIAL { get; set; }
        public string NOME_FANTASIA { get; set; }
        public string CNPJ { get; set; }
        public string INSCR_ESTADUAL { get; set; }
        public string TEL_COMERCIAL { get; set; }
        public string CADASTRADO_POR { get; set; }
        public DateTime DATA_CADASTRO { get; set; }
        public string ALTERADO_POR { get; set; }
        public DateTime DATA_ALTERACAO { get; set; }

        public virtual HB_PEDIDO_CAPA HB_PEDIDO_CAPA { get; set; }
    }
}
