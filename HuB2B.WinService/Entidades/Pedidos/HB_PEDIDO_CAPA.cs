﻿using HuB2B.WinService.Entidades.HUB2B;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace HuB2B.WinService.Entidades.Pedidos
{
    public class HB_PEDIDO_CAPA
    {
        public HB_PEDIDO_CAPA()
        {
            DATA_ALTERACAO = DateTime.Now;
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }
        public string NUM_PEDIDO { get; set; }
        public string NUM_MARKETPLACE { get; set; }
        public Int64 SEQUENCIAL_HUB2B { get; set; }
        public string IDENT_PEDIDO { get; set; }

        public int CANALVENDAID { get; set; }
        [ForeignKey("CANALVENDAID")]
        public virtual HB_CANAL_VENDAS HB_CANAL_VENDAS { get; set; }

        public string CANAL_ORIGEM { get; set; }
        public string CANAL_SITE { get; set; }
        public string CANAL_PREFIXO { get; set; }
        public string STATUS_PEDIDO { get; set; }
        public string STATUS_DESCR { get; set; }
        public decimal VALOR_TOTAL { get; set; }
        public decimal DESCONTO_TOTAL { get; set; }

        public DateTime DATA_EMISSAO { get; set; }
        public DateTime? DATA_CANCELAMENTO { get; set; }
        public bool? DOWNLOAD { get; set; }
        public string CADASTRADO_POR { get; set; }
        public DateTime DATA_CADASTRO { get; set; }
        public string ALTERADO_POR { get; set; }
        public DateTime DATA_ALTERACAO { get; set; }

        public virtual HB_PEDIDO_CLIENTE HB_PEDIDO_CLIENTE { get; set; }
        public virtual HB_PEDIDO_ENDERECO HB_PEDIDO_ENDERECO { get; set; }
        public virtual ICollection<HB_PEDIDO_ITEM> HB_PEDIDO_ITEMs { get; set; }
        public virtual ICollection<HB_PEDIDO_LOGISTICA> HB_PEDIDO_LOGISTICAs { get; set; }
        public virtual ICollection<HB_PEDIDO_PAGAMENTO> HB_PEDIDO_PAGAMENTOs { get; set; }
        public virtual ICollection<HB_PEDIDO_RASTREAMENTO> HB_PEDIDO_RASTREAMENTOs { get; set; }
    }
}
