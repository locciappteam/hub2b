﻿namespace HuB2B.WinService
{
    partial class ProjectInstaller
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.serviceProcessInstallerHub2b = new System.ServiceProcess.ServiceProcessInstaller();
            this.serviceInstallerHub2b = new System.ServiceProcess.ServiceInstaller();
            // 
            // serviceProcessInstallerHub2b
            // 
            this.serviceProcessInstallerHub2b.Account = System.ServiceProcess.ServiceAccount.LocalSystem;
            this.serviceProcessInstallerHub2b.Password = null;
            this.serviceProcessInstallerHub2b.Username = null;
            // 
            // serviceInstallerHub2b
            // 
            this.serviceInstallerHub2b.Description = "Consome o WebAPI da HuB2B";
            this.serviceInstallerHub2b.DisplayName = "LOcc - HuB2B";
            this.serviceInstallerHub2b.ServiceName = "ServiceHub2b";
            this.serviceInstallerHub2b.AfterInstall += new System.Configuration.Install.InstallEventHandler(this.serviceInstaller1_AfterInstall);
            // 
            // ProjectInstaller
            // 
            this.Installers.AddRange(new System.Configuration.Install.Installer[] {
            this.serviceProcessInstallerHub2b,
            this.serviceInstallerHub2b});

        }

        #endregion

        private System.ServiceProcess.ServiceProcessInstaller serviceProcessInstallerHub2b;
        private System.ServiceProcess.ServiceInstaller serviceInstallerHub2b;
    }
}