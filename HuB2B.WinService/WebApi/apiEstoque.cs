﻿using HuB2B.WinService.Data;
using HuB2B.WinService.Entidades.Estoque;
using HuB2B.WinService.Entidades.HUB2B;
using HuB2B.WinService.Entidades.Integracao;
using HuB2B.WinService.Json.Estoque;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace HuB2B.WinService.WebApi
{
    public class apiEstoque
    {
        static HuB2BDataContext _ctx = new HuB2BDataContext();
        static ViewsDataContext _views = new ViewsDataContext();

        public static async Task AtualizarEstoque(Uri baseAddress, string IdTenant, string ChaveAuth)
        {
            Log.Write(String.Format($"{DateTime.Now} : WebApi - Atualizar Estoque"));

            _ctx.Database.CommandTimeout = 300;
            _views.CommandTimeout = 300;
            List<RequestEstoque> wHbEstoque = (
                from
                    a in _views.W_HB_ESTOQUE_PRODUTOs
                select new RequestEstoque
                {
                    itemId = a.COD_PRODUTO,
                    quantity = a.QTDE,
                    wareHouseId = 1,
                    crossdocking = 8
                }
            ).ToList();


            for (int i = 0; i < wHbEstoque.Count; i+=50)
            {
                string JsonRequestEstoque = JsonConvert.SerializeObject(wHbEstoque.Skip(i).Take(50));

                using (var httpClient = new HttpClient { BaseAddress = baseAddress })
                {
                    httpClient.Timeout = TimeSpan.FromMinutes(10);

                    httpClient.DefaultRequestHeaders.TryAddWithoutValidation("auth", ChaveAuth);

                    using (var content = new StringContent(JsonRequestEstoque, System.Text.Encoding.Default, "application/json"))
                    {
                        string requestUri = string.Format("setstock/{0}", IdTenant);

                        using (var response = await httpClient.PostAsync(requestUri, content))
                        {
                            string responseData = await response.Content.ReadAsStringAsync();

                            if (response.IsSuccessStatusCode)
                            {
                                ResponseEstoque responseEstoque = JsonConvert.DeserializeObject<ResponseEstoque>(responseData);

                                HB_ESTOQUE_CONTROLE HbEstoqueControle = new HB_ESTOQUE_CONTROLE
                                {
                                    ESTABELECIMENTOID = 1, // FICTICIO, ESTOU SAINDO E NÃO QUERO FAZER GRANDES ALTERAÇÕES NO CÓDIGO
                                    CROSSDOCKING = 8,
                                    CADASTRADO_POR = "INTEGRACAO",
                                    DATA_CADASTRO = DateTime.Now,
                                    ALTERADO_POR = "INTEGRACAO",
                                    DATA_ALTERACAO = DateTime.Now,
                                    HB_ESTOQUE_ITEMs = new List<HB_ESTOQUE_ITEM>(),
                                    HB_ESTOQUE_RETORNOs = new List<HB_ESTOQUE_RETORNO>()
                                };

                                //List<HB_ESTOQUE_ITEM> HbEstoqueItems = new List<HB_ESTOQUE_ITEM>();
                                //foreach (var Item in wHbEstoque.Where(a => a.ESTABELECIMENTOID == Estabelecimento.ESTABELECIMENTOID))
                                //{
                                //    HbEstoqueItems.Add(new HB_ESTOQUE_ITEM
                                //    {
                                //        COD_PRODUTO = Item.COD_PRODUTO,
                                //        QTDE = Item.QTDE,
                                //        CADASTRADO_POR = "INTEGRACAO",
                                //        DATA_CADASTRO = DateTime.Now,
                                //        ALTERADO_POR = "INTEGRACAO",
                                //        DATA_ALTERACAO = DateTime.Now
                                //    });
                                //}

                                HB_ESTOQUE_RETORNO HbEstoqueRetorno = new HB_ESTOQUE_RETORNO
                                {
                                    COD_RETORNO = response.StatusCode.ToString(),
                                    DESC_ERRO = responseEstoque.error,
                                    DESC_DATA = responseEstoque.data,
                                    CADASTRADO_POR = "INTEGRACAO",
                                    DATA_CADASTRO = DateTime.Now,
                                    ALTERADO_POR = "INTEGRCAO",
                                    DATA_ALTERACAO = DateTime.Now
                                };

                                //HbEstoqueControle.HB_ESTOQUE_ITEMs = HbEstoqueItems;
                                HbEstoqueControle.HB_ESTOQUE_RETORNOs.Add(HbEstoqueRetorno);

                                _ctx.Set<HB_ESTOQUE_CONTROLE>().Add(HbEstoqueControle);
                                _ctx.SaveChanges();
                            }
                            else
                            {
                                HB_RETORNO_LOG HbRetornoLog = new HB_RETORNO_LOG
                                {
                                    COD_RETORNO = response.StatusCode.ToString(),
                                    DESC_RETORNO = "Erro na requisição!",
                                    INTEGRACAO = "AtualizarEstoque",
                                    CADASTRADO_POR = "INTEGRACAO",
                                    DATA_CADASTRO = DateTime.Now,
                                    ALTERADO_POR = "INTEGRACAO",
                                    DATA_ALTERACAO = DateTime.Now
                                };

                                _ctx.Set<HB_RETORNO_LOG>().Add(HbRetornoLog);
                                _ctx.SaveChanges();
                            }
                        }
                    }
                }

            }

        }
    }
}
