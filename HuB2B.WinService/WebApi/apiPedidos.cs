﻿using HuB2B.WinService.Data;
using HuB2B.WinService.Entidades.HUB2B;
using HuB2B.WinService.Entidades.Pedidos;
using HuB2B.WinService.Json.Pedido;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net.Http;
using System.Security.AccessControl;
using System.Threading.Tasks;

namespace HuB2B.WinService.WebApi
{
    public class apiPedidos
    {
        static  HuB2BDataContext _ctx;
        static ViewsDataContext _views = new ViewsDataContext();

        public static async Task PedidosID(Uri baseAddress, string IdTenant, string ChaveAuth)
        {
            //_ctx.Database.CommandTimeout = 300; 

            Log.Write(String.Format($"{DateTime.Now} : WebApi - Obtendo ID dos pedidos"));

            using (var httpClient = new HttpClient { BaseAddress = baseAddress })                 
            {
                _ctx = new HuB2BDataContext();

                httpClient.Timeout = TimeSpan.FromMinutes(10);

                httpClient.DefaultRequestHeaders.TryAddWithoutValidation("auth", ChaveAuth);
                httpClient.DefaultRequestHeaders.Add("User-Agent", "C# App");

                string requestUri = string.Format("listordersid/{0}?status={1}", IdTenant, "payment-approved");
                 
                using (var response = await httpClient.GetAsync(requestUri))
                {
                    string responseData = await response.Content.ReadAsStringAsync();

                    if (response.IsSuccessStatusCode)
                    {
                        ResponsePedidoId responsePedidoId = JsonConvert.DeserializeObject<ResponsePedidoId>(responseData);

                            foreach (var PedidoId in responsePedidoId.data.list)
                            {
                                Log.Write(String.Format($"{DateTime.Now} :: WebApi - Obtendo ID {PedidoId.orderId}"));

                                HB_PEDIDO_ID _HbPedidoId = _ctx.Set<HB_PEDIDO_ID>().AsNoTracking().FirstOrDefault(a => a.NUM_PEDIDO == PedidoId.orderId);
                                if (_HbPedidoId == null)
                                {
                                    HB_PEDIDO_ID HbPedidoId = new HB_PEDIDO_ID
                                    {
                                        NUM_PEDIDO = PedidoId.orderId,
                                        NUM_MARKETPLACE = PedidoId.marketplaceOrderId,
                                        CANAL_VENDA_PREFIXO = PedidoId.affiliateId,
                                        CANALVENDAID = _ctx.Set<HB_CANAL_VENDAS>().Where(a => a.COD_CANAL == PedidoId.salesChannel).Select(a => a.ID).First(),
                                        CADASTRADO_POR = "INTEGRACAO",
                                        DATA_CADASTRO = DateTime.Now,
                                        ALTERADO_POR = "INTEGRACAO",
                                        DATA_ALTERACAO = DateTime.Now
                                    };

                                    _ctx.Set<HB_PEDIDO_ID>().Add(HbPedidoId);
                                    _ctx.SaveChanges();
                                }
                            }
                        }
                    else
                    {
                             HB_RETORNO_LOG HbRetornoLog = new HB_RETORNO_LOG
                            {
                                COD_RETORNO = response.StatusCode.ToString(),
                                DESC_RETORNO = "Erro na requisição!",
                                INTEGRACAO = "PedidosID",
                                CADASTRADO_POR = "INTEGRACAO",
                                DATA_CADASTRO = DateTime.Now,
                                ALTERADO_POR = "INTEGRACAO",
                                DATA_ALTERACAO = DateTime.Now
                            };

                            _ctx.Set<HB_RETORNO_LOG>().Add(HbRetornoLog);
                            _ctx.SaveChanges();
                    }
                }
            }
        }

        public static async Task Pedidos(Uri baseAddress, string IdTenant, string ChaveAuth)
        {
            _ctx = new HuB2BDataContext();
            _ctx.Database.CommandTimeout = 300;

            Log.Write(String.Format($"{DateTime.Now} : WebApi - Obtendo Pedidos"));

            List<string> PedidosID = (
                from a in _ctx.HbPedidoId
                from b in _ctx.HbPedidoCapa.Where(capa => capa.NUM_PEDIDO == a.NUM_PEDIDO).DefaultIfEmpty()
                orderby a.DATA_CADASTRO descending
                where
                    b.DOWNLOAD == false || b.DOWNLOAD == null
                select
                    a.NUM_PEDIDO
            ).ToList();


            using (var httpClient = new HttpClient { BaseAddress = baseAddress })
            {
                httpClient.Timeout = TimeSpan.FromMinutes(10);
                httpClient.DefaultRequestHeaders.TryAddWithoutValidation("auth", ChaveAuth);
                httpClient.DefaultRequestHeaders.Add("User-Agent", "C# App");

                foreach (var Pedido in PedidosID)
                {
                    Log.Write(String.Format($"{DateTime.Now} : Obtendo Pedidos - Pedido: {Pedido}"));
                                                        
                    try
                    {
                    string requestUri = string.Format("order/{0}?order={1}", IdTenant, Pedido);
                    using (var response = await httpClient.GetAsync(requestUri))
                    {
                        string responseData = await response.Content.ReadAsStringAsync();

                        if (response.IsSuccessStatusCode)
                        {
                            ResponsePedido responsePedido = JsonConvert.DeserializeObject<ResponsePedido>(responseData);

                            HB_PEDIDO_CAPA _HbPedidoCapa = _ctx.Set<HB_PEDIDO_CAPA>().Where(a => a.NUM_PEDIDO == responsePedido.data.orderId).FirstOrDefault();
                            if (_HbPedidoCapa == null)
                            {
                                HB_PEDIDO_CAPA HbPedidoCapa = new HB_PEDIDO_CAPA
                                {
                                    NUM_PEDIDO = responsePedido.data.orderId,
                                    NUM_MARKETPLACE = responsePedido.data.marketplaceOrderId,
                                    SEQUENCIAL_HUB2B = responsePedido.data.sequence,
                                    IDENT_PEDIDO = responsePedido.data.sellerOrderId,
                                    CANALVENDAID = _ctx.Set<HB_CANAL_VENDAS>().Where(a => a.COD_CANAL == responsePedido.data.salesChannel).Select(a => a.ID).First(),
                                    CANAL_ORIGEM = responsePedido.data.origin,
                                    CANAL_SITE = responsePedido.data.marketplaceSite,
                                    CANAL_PREFIXO = responsePedido.data.affiliateId,
                                    STATUS_PEDIDO = responsePedido.data.status,
                                    STATUS_DESCR = responsePedido.data.statusDescription,
                                    VALOR_TOTAL = responsePedido.data.value / (decimal)100,
                                    DESCONTO_TOTAL = responsePedido.data.discount / (decimal)100,
                                    DATA_EMISSAO = responsePedido.data.creationDate,
                                    DATA_CANCELAMENTO = responsePedido.data.canceledDate,
                                    DOWNLOAD = false,
                                    CADASTRADO_POR = "INTEGRACAO",
                                    DATA_CADASTRO = DateTime.Now,
                                    ALTERADO_POR = "INTEGRACAO",
                                    DATA_ALTERACAO = DateTime.Now,
                                    HB_PEDIDO_CLIENTE = new HB_PEDIDO_CLIENTE
                                    {
                                        NOME = responsePedido.data.clientProfileData.firstName,
                                        SOBRENOME = responsePedido.data.clientProfileData.lastName,
                                        EMAIL = responsePedido.data.clientProfileData.email,
                                        TIPO_DOCUMENTO = responsePedido.data.clientProfileData.documentType,
                                        NUM_DOCUMENTO = responsePedido.data.clientProfileData.document,
                                        TELEFONE = responsePedido.data.clientProfileData.phone,
                                        PESSOA_JURIDICA = responsePedido.data.clientProfileData.isCorporate,
                                        RAZAO_SOCIAL = responsePedido.data.clientProfileData.corporateName,
                                        NOME_FANTASIA = responsePedido.data.clientProfileData.tradeName,
                                        CNPJ = responsePedido.data.clientProfileData.corporateDocument,
                                        INSCR_ESTADUAL = responsePedido.data.clientProfileData.stateInscription,
                                        TEL_COMERCIAL = responsePedido.data.clientProfileData.corporatePhone,
                                        CADASTRADO_POR = "INTEGRACAO",
                                        DATA_CADASTRO = DateTime.Now,
                                        ALTERADO_POR = "INTEGRACAO",
                                        DATA_ALTERACAO = DateTime.Now
                                    },
                                    HB_PEDIDO_ENDERECO = new HB_PEDIDO_ENDERECO
                                    {
                                        ENDERECO = responsePedido.data.shippingData.address.street,
                                        NUMERO = responsePedido.data.shippingData.address.number,
                                        CEP = responsePedido.data.shippingData.address.postalCode,
                                        CIDADE = responsePedido.data.shippingData.address.city,
                                        ESTADO = responsePedido.data.shippingData.address.state,
                                        BAIRRO = responsePedido.data.shippingData.address.neighborhood,
                                        COMPLEMENTO = responsePedido.data.shippingData.address.complement,
                                        CODIGO_IBGE = responsePedido.data.shippingData.address.cityIBGE,
                                        PAIS = responsePedido.data.shippingData.address.country,
                                        NOME_RECEBEDOR = responsePedido.data.shippingData.address.receiverName,
                                        CADASTRADO_POR = "INTEGRACAO",
                                        DATA_CADASTRO = DateTime.Now,
                                        ALTERADO_POR = "INTEGRACAO",
                                        DATA_ALTERACAO = DateTime.Now
                                    },
                                    HB_PEDIDO_ITEMs = new List<HB_PEDIDO_ITEM>(),
                                    HB_PEDIDO_PAGAMENTOs = new List<HB_PEDIDO_PAGAMENTO>(),
                                    HB_PEDIDO_LOGISTICAs = new List<HB_PEDIDO_LOGISTICA>(),
                                    HB_PEDIDO_RASTREAMENTOs = new List<HB_PEDIDO_RASTREAMENTO>()
                                };

                                foreach (var item in responsePedido.data.items)
                                {
                                    HbPedidoCapa.HB_PEDIDO_ITEMs.Add(new HB_PEDIDO_ITEM
                                    {
                                        COD_PRODUTO = item.id,
                                        QUANTIDADE = item.quantity,
                                        VALOR_UNITARIO = item.sellingPrice / (decimal)100,
                                        VALOR_DESCONTO = item.discount / (decimal)100,
                                        XML_GERADO = false,
                                        CADASTRADO_POR = "INTEGRACAO",
                                        DATA_CADASTRO = DateTime.Now,
                                        ALTERADO_POR = "INTEGRACAO",
                                        DATA_ALTERACAO = DateTime.Now
                                    });
                                }
                                
                                foreach (var Transacao in responsePedido.data.paymentData.transactions)
                                {
                                    foreach (var Pagamento in Transacao.payments)
                                    {
                                        HbPedidoCapa.HB_PEDIDO_PAGAMENTOs.Add(new HB_PEDIDO_PAGAMENTO
                                        {
                                            NUMERO_PARCELAS = Pagamento.installments,
                                            METODO_PAGAMENTO = Pagamento.paymentSystem,
                                            VALOR_PAGAMENTO = Pagamento.value / (decimal)100,
                                            CADASTRADO_POR = "INTEGRACAO",
                                            DATA_CADASTRO = DateTime.Now,
                                            ALTERADO_POR = "INTEGRACAO",
                                            DATA_ALTERACAO = DateTime.Now
                                        });
                                    }
                                }

                                foreach (var Logistica in responsePedido.data.shippingData.logisticsInfo)
                                {
                                    foreach (var Entrega in Logistica.deliveryIds)
                                    {
                                        HbPedidoCapa.HB_PEDIDO_LOGISTICAs.Add(new HB_PEDIDO_LOGISTICA
                                        {
                                            DIAS_EST_ENTREGA = Logistica.shippingEstimate.ToString(),
                                            DATA_EST_ENTREGA = Logistica.shippingEstimateDate,
                                            VALOR_FRETE = Logistica.sellingPrice / (decimal)100,
                                            COD_TRANSPORTADORA = Entrega.courierId,
                                            NOME_TRANSPORTADORA = Entrega.courierName,
                                            QTDE_ENVIADA = Entrega.quantity,
                                            ARMAZEMID = Entrega.warehouseId,
                                            CADASTRADO_POR = "INTEGRACAO",
                                            DATA_CADASTRO = DateTime.Now,
                                            ALTERADO_POR = "INTEGRACAO",
                                            DATA_ALTERACAO = DateTime.Now
                                        });
                                    }
                                }

                                foreach (var Rastreamento in responsePedido.data.packageAttachment.packages)
                                {
                                    HbPedidoCapa.HB_PEDIDO_RASTREAMENTOs.Add(new HB_PEDIDO_RASTREAMENTO
                                    {
                                        NUMERO_NFE = Rastreamento.invoiceNumber,
                                        SERIE_NFE = Rastreamento.invoiceSeries,
                                        CHAVE_NFE = Rastreamento.invoiceKey,
                                        DATA_EMISSAO = Rastreamento.invoiceIssueDate,
                                        CODIGO_RASTREIO = Rastreamento.trackingCode,
                                        URL_RASTREIO = Rastreamento.trackingURL,
                                        CADASTRADO_POR = "INTEGRACAO",
                                        DATA_CADASTRO = DateTime.Now,
                                        ALTERADO_POR = "INTEGRACAO",
                                        DATA_ALTERACAO = DateTime.Now
                                    });
                                }

                                _ctx.Set<HB_PEDIDO_CAPA>().Add(HbPedidoCapa);
                                _ctx.SaveChanges();
 
                                Log.Write(String.Format($"{DateTime.Now} ::: Pedido Salvo - {Pedido}"));

                                }
                            }
                        else
                        {
                            HB_RETORNO_LOG HbRetornoLog = new HB_RETORNO_LOG
                            {
                                COD_RETORNO = response.StatusCode.ToString(),
                                DESC_RETORNO = "Erro na requisição!",
                                INTEGRACAO = "Pedidos",
                                CADASTRADO_POR = "INTEGRACAO",
                                DATA_CADASTRO = DateTime.Now,
                                ALTERADO_POR = "INTEGRACAO",
                                DATA_ALTERACAO = DateTime.Now
                            };

                            _ctx.Set<HB_RETORNO_LOG>().Add(HbRetornoLog);
                            _ctx.SaveChanges();
                        }
                    }
                   }
                    catch (Exception ex)
                    {
                        Log.Write(String.Format($"{DateTime.Now} : \n ========== ERRO FATAL ========== \n {ex.ToString()}"));
                        //Email.Enviar("am03svc-cegid@loccitane.com.br", "wagner.damasio@loccitane.com", ex.ToString());
                                
                        continue;
                    }
                }
            }
        }

        public static async Task ConfirmaDownload(Uri baseAddress, string IdTenant, string ChaveAuth)
        {
            _ctx = new HuB2BDataContext();
            _ctx.Database.CommandTimeout = 300;

            Log.Write(String.Format($"{DateTime.Now} : WebApi - Confirmando Download dos Pedidos"));

            List<string> PedidosDownload = (
                from
                    a in _ctx.HbPedidoCapa
                where
                    a.DOWNLOAD == false
                select
                    a.NUM_PEDIDO
            ).ToList();

            List<RequestPedidoDownload> RequestPedidoDownload = new List<RequestPedidoDownload>();
            foreach (var Pedido in PedidosDownload)
            {
                RequestPedidoDownload requestPedidoDownload = new RequestPedidoDownload
                {
                    id = Pedido
                };

                RequestPedidoDownload.Add(requestPedidoDownload);
            }

            if (RequestPedidoDownload.Any())
            {
                string RequestPedidoDownloadJson = JsonConvert.SerializeObject(RequestPedidoDownload);

                using (var httpClient = new HttpClient { BaseAddress = baseAddress })
                {
                    httpClient.Timeout = TimeSpan.FromMinutes(10);
                    httpClient.DefaultRequestHeaders.TryAddWithoutValidation("auth", ChaveAuth);
                    httpClient.DefaultRequestHeaders.Add("User-Agent", "C# App");

                    using (var content = new StringContent(RequestPedidoDownloadJson, System.Text.Encoding.Default, "application/json"))
                    {
                        string requestUri = string.Format("confirmorders/{0}", IdTenant);

                        using (var response = await httpClient.PostAsync(requestUri, content))
                        {
                            string responseData = await response.Content.ReadAsStringAsync();

                            if (response.IsSuccessStatusCode)
                            {
                                List<HB_PEDIDO_CAPA> PedidosSelecionados = (
                                    from
                                        a in _ctx.HbPedidoCapa
                                    where
                                        PedidosDownload.Contains(a.NUM_PEDIDO)
                                    select
                                        a
                                ).ToList();

                               foreach (var PedidoSelecionado in PedidosSelecionados)
                                {
                                    try
                                    {
                                        PedidoSelecionado.DOWNLOAD = true;

                                        _ctx.Entry(PedidoSelecionado).State = EntityState.Modified;
                                        _ctx.SaveChanges();

                                        Log.Write(String.Format($"{DateTime.Now} ::: Download Confirmado - {PedidoSelecionado.NUM_PEDIDO}"));
                                    }
                                    catch (Exception ex)
                                    {
                                        Log.Write(String.Format($"{DateTime.Now} : \n ========== ERRO FATAL ========== \n {ex.ToString()}"));
                                    }
                                }
                            }
                            else
                            {
                                HB_RETORNO_LOG HbRetornoLog = new HB_RETORNO_LOG
                                {
                                    COD_RETORNO = response.StatusCode.ToString(),
                                    DESC_RETORNO = "Erro na requisição!",
                                    INTEGRACAO = "ConfirmaDownload",
                                    CADASTRADO_POR = "INTEGRACAO",
                                    DATA_CADASTRO = DateTime.Now,
                                    ALTERADO_POR = "INTEGRACAO",
                                    DATA_ALTERACAO = DateTime.Now
                                };

                                _ctx.Set<HB_RETORNO_LOG>().Add(HbRetornoLog);
                                _ctx.SaveChanges();
                            }
                        }
                    }
                }
            }
        }

        public static async Task AtualizarFaturamento(Uri baseAddress, string IdTenant, string ChaveAuth)
        {
 
            Log.Write(String.Format($"{DateTime.Now} : WebApi - Atualizando Faturamento"));

            _views.CommandTimeout = 300;
            DateTime DataMinima = DateTime.Today.AddMonths(-12);
            //DateTime DataMaxima = DateTime.Today.AddMonths(-1);

            List<W_HB_PEDIDO_FATURADO> wHbPedidoFaturado = (
                from
                    a in _views.W_HB_PEDIDO_FATURADOs
                where
                a.DATA_EMISSAO >= DataMinima //&& a.DATA_EMISSAO <= DataMaxima
                && a.ENVIO_FATURADO == false
                //&& a.NUM_PEDIDO.Contains("daf")
               
                orderby a.NUM_PEDIDO descending
                select
                    a
            ).ToList();

            foreach (var Pedido in wHbPedidoFaturado.Select(a => new { a.PEDIDOID, a.NUMERO_NF, a.CPF_CNPJ, a.DATA_EMISSAO, a.VLR_TOT_NOTA, a.CHAVE_NFE, a.SERIE_NF, a.NUM_PEDIDO }).Distinct())
            {
                try
                {
                    RequestPedidoFaturado PedidoFaturado = new RequestPedidoFaturado
                    {
                        type = "Output",
                        invoiceNumber = Pedido.NUMERO_NF+Pedido.CPF_CNPJ,
                        courier = "Total Express",
                        service = "PAC",

                        //trackingNumber = Pedido.CPF_CNPJ,

                        // 04/2021 Realizda troca de envio do CPF/CNPJ PELO NUMERO_NF: PROBLEMAS DE DUPLICIDADE DAFIT
                        trackingNumber = Pedido.NUMERO_NF,
                        trackingUrl = @"http://tracking.totalexpress.com.br/tracking/0?cpf_cnpj",
                        issuanceDate = Pedido.DATA_EMISSAO,
                        invoiceValue = Pedido.VLR_TOT_NOTA,
                        invoiceKey = Pedido.CHAVE_NFE,
                        invoiceSeries = Pedido.SERIE_NF,
                        cfop = "",

                        items = new List<ItemPedidoFaturado>()
                    };

                    foreach (var Produtos in wHbPedidoFaturado.Where(a => a.NUM_PEDIDO == Pedido.NUM_PEDIDO))
                    {
                        PedidoFaturado.items.Add(new ItemPedidoFaturado
                        {
                            id = Produtos.COD_PRODUTO,
                            price = Produtos.VLR_LIQ_ITEM,
                            quantity = Produtos.QTDE_FATURADA
                        });
                    }

                    List<RequestPedidoFaturado> ListRequestPedidoFaturado = new List<RequestPedidoFaturado>();
                    ListRequestPedidoFaturado.Add(PedidoFaturado);

                    string RequestPedidoFaturadoJson = JsonConvert.SerializeObject(ListRequestPedidoFaturado);

                    using (var httpClient = new HttpClient { BaseAddress = baseAddress })
                    {

                        httpClient.Timeout = TimeSpan.FromMinutes(10);
                        httpClient.DefaultRequestHeaders.TryAddWithoutValidation("auth", ChaveAuth);
                        httpClient.DefaultRequestHeaders.Add("User-Agent", "C# App");

                        using (var content = new StringContent(RequestPedidoFaturadoJson, System.Text.Encoding.Default, "application/json"))
                        {
                            string requestUri = string.Format("invoiceorder/{0}?order={1}", IdTenant, Pedido.NUM_PEDIDO);

                            using (var response = await httpClient.PostAsync(requestUri, content))
                            {
                                string responseData = await response.Content.ReadAsStringAsync();                                 

                                if (response.IsSuccessStatusCode)
                                {
                                    Log.Write(String.Format($"{DateTime.Now} : Atualizando Faturamento - Pedido {Pedido.NUM_PEDIDO}"));
                                                 
                                    ResponseAtualizarFaturamento responseAtualizarFaturamento = JsonConvert.DeserializeObject<ResponseAtualizarFaturamento>(responseData);
                                    foreach (var Retorno in responseAtualizarFaturamento.data.list)
                                    {
                                        foreach (var Produto in wHbPedidoFaturado.Where(a => a.NUM_PEDIDO == Pedido.NUM_PEDIDO))
                                        {
                                            HB_PEDIDO_FATURADO HbPedidoFaturado = new HB_PEDIDO_FATURADO
                                            {
                                                ID = Produto.PRODUTOID,
                                                DATA_RECEBIMENTO = Retorno.date,
                                                COD_RECEPTOR = Retorno.receipt,
                                                DESC_RETORNO = "Retorno com sucesso!",
                                                CADASTRADO_POR = "INTEGRACAO",
                                                DATA_CADASTRO = DateTime.Now,
                                                ALTERADO_POR = "INTEGRACAO",
                                                DATA_ALTERACAO = DateTime.Now
                                            };

                                            using (var _dbx = new HuB2BDataContext())
                                            {
                                                if (!_dbx.HbPedidoFaturado.Where(p => p.ID == HbPedidoFaturado.ID).AsNoTracking().Any())
                                                {
                                                    _dbx.Set<HB_PEDIDO_FATURADO>().Add(HbPedidoFaturado);
                                                    _dbx.SaveChanges();
                                                }
                                            }
                                        }
                                    }
                                }
                                else
                                {

                                    using (var _dbx = new HuB2BDataContext())
                                    {
                                        HB_RETORNO_LOG HbRetornoLog = new HB_RETORNO_LOG
                                        {
                                            COD_RETORNO = response.StatusCode.ToString(),
                                            DESC_RETORNO = "Erro na requisição!",
                                            INTEGRACAO = "AtualizarFaturamento",
                                            CADASTRADO_POR = "INTEGRACAO",
                                            DATA_CADASTRO = DateTime.Now,
                                            ALTERADO_POR = "INTEGRACAO",
                                            DATA_ALTERACAO = DateTime.Now
                                        };

                                        _dbx.Set<HB_RETORNO_LOG>().Add(HbRetornoLog);
                                        _dbx.SaveChanges();
                                    }
                                }
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    Log.Write(String.Format($"{DateTime.Now} : \n ========== ERRO FATAL ========== \n {ex.ToString()}"));
                    //Email.Enviar("am03svc-cegid@loccitane.com.br", "wagner.damasio@loccitane.com", ex.ToString());
                }
            }
        }
    }
}