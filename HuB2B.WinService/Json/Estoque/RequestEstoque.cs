﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HuB2B.WinService.Json.Estoque
{
    public class RequestEstoque
    {
        public string itemId { get; set; }
        public int? quantity { get; set; }
        public int? wareHouseId { get; set; }
        public int crossdocking { get; set; }
    }
}
