﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HuB2B.WinService.Json.Estoque
{
    public class ResponseEstoque
    {
        public string error { get; set; }
        public string data { get; set; }
    }
}
