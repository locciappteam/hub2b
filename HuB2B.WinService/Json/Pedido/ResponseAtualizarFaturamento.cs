﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HuB2B.WinService.Json.Pedido
{
    public class ResponseAtualizarFaturamento
    {
        public string error { get; set; }
        public Data data { get; set; }
    }

    public class List
    {
        public DateTime date { get; set; }
        public string orderId { get; set; }
        public string receipt { get; set; }
    }

    public class Data
    {
        public List<List> list { get; set; }
    }
}
