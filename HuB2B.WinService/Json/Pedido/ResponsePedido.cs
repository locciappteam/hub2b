﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HuB2B.WinService.Json.Pedido
{
    public class ResponsePedido
    {
        public string error { get; set; }
        public PedidoData data { get; set; }
    }

    public class Item
    {
        public string id { get; set; }
        public int quantity { get; set; }
        public int discount { get; set; }
        public int sellingPrice { get; set; }
    }

    public class ClientProfileData
    {
        public string email { get; set; }
        public string firstName { get; set; }
        public string lastName { get; set; }
        public string documentType { get; set; }
        public string document { get; set; }
        public string phone { get; set; }
        public bool isCorporate { get; set; }
        public string corporateName { get; set; }
        public string tradeName { get; set; }
        public string corporateDocument { get; set; }
        public string stateInscription { get; set; }
        public string corporatePhone { get; set; }
    }

    public class Package
    {
        public string invoiceNumber { get; set; }
        public string invoiceSeries { get; set; }
        public string invoiceKey { get; set; }
        public DateTime invoiceIssueDate { get; set; }
        public string trackingCode { get; set; }
        public string trackingURL { get; set; }
    }

    public class PackageAttachment
    {
        public List<Package> packages { get; set; }
    }

    public class Payment
    {
        public int installments { get; set; }
        public string paymentSystem { get; set; }
        public int value { get; set; }
    }

    public class Transaction
    {
        public List<Payment> payments { get; set; }
    }

    public class PaymentData
    {
        public List<Transaction> transactions { get; set; }
    }

    public class MarketingData
    {
        public string utmSource { get; set; }
    }

    public class Address
    {
        public string receiverName { get; set; }
        public string postalCode { get; set; }
        public string city { get; set; }
        public int? cityIBGE { get; set; }
        public string state { get; set; }
        public string country { get; set; }
        public string street { get; set; }
        public string number { get; set; }
        public string neighborhood { get; set; }

        [JsonIgnore]
        public string complement { get; set; }

        [JsonProperty("complement")]
        public string ComplementStr
        {
            get
            {
                return complement;
            }
            set
            {
                complement = value.Substring(0, value.IndexOf("\n") <= 0 ? value.Length : value.IndexOf("\n") - 1);
            }
        }
    }

    public class DeliveryId
    {
        public string courierId { get; set; }
        public string courierName { get; set; }
        public int quantity { get; set; }
        public int warehouseId { get; set; }
    }

    public class LogisticsInfo
    {
        public decimal shippingEstimate { get; set; }
        public DateTime shippingEstimateDate { get; set; }
        public int sellingPrice { get; set; }
        public List<DeliveryId> deliveryIds { get; set; }
    }

    public class ShippingData
    {
        public Address address { get; set; }
        public List<LogisticsInfo> logisticsInfo { get; set; }
    }

    public class PedidoData
    {
        public string orderId { get; set; }
        public Int64 sequence { get; set; }
        public string marketplaceOrderId { get; set; }
        public string marketplaceServicesEndpoint { get; set; }
        public string sellerOrderId { get; set; }
        public string origin { get; set; }
        public string marketplaceSite { get; set; }
        public string affiliateId { get; set; }
        public int salesChannel { get; set; }
        public string status { get; set; }
        public string statusDescription { get; set; }
        public int value { get; set; }
        public int discount { get; set; }
        public dynamic creationDate { get; set; }
        public dynamic canceledDate { get; set; }
        public List<Item> items { get; set; }
        public ClientProfileData clientProfileData { get; set; }
        public PackageAttachment packageAttachment { get; set; }
        public PaymentData paymentData { get; set; }
        public MarketingData marketingData { get; set; }
        public ShippingData shippingData { get; set; }
    }
}
