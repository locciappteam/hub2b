﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HuB2B.WinService.Json.Pedido
{
    public class ResponsePedidoId
    {
        public string error { get; set; }
        public PedidoIdData data { get; set; }
        public Paging paging { get; set; }
    }

    public class ListData
    {
        public string orderId { get; set; }
        public string marketplaceOrderId { get; set; }
        public string affiliateId { get; set; }
        public int salesChannel { get; set; }
    }

    public class PedidoIdData
    {
        public List<ListData> list { get; set; }
    }

    public class Paging
    {
        public int total { get; set; }
        public int pages { get; set; }
        public int currentPage { get; set; }
        public int perPage { get; set; }
    }   
}
