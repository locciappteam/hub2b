﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HuB2B.WinService.Json.Pedido
{
    public class ItemPedidoFaturado
    {
        public string id { get; set; }
        public int? quantity { get; set; }
        public decimal? price { get; set; }
    }

    public class RequestPedidoFaturado
    {
        public string type { get; set; }
        public string invoiceNumber { get; set; }
        public string courier { get; set; }
        public string service { get; set; }
        public string trackingNumber { get; set; }
        public string trackingUrl { get; set; }
        public List<ItemPedidoFaturado> items { get; set; }
        public DateTime? issuanceDate { get; set; }
        public decimal? invoiceValue { get; set; }
        public string invoiceKey { get; set; }
        public string invoiceSeries { get; set; }
        public string cfop { get; set; }
    }
}
