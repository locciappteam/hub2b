﻿using HuB2B.WinService.Entidades.Estoque;
using HuB2B.WinService.Entidades.HUB2B;
using HuB2B.WinService.Entidades.Integracao;
using HuB2B.WinService.Entidades.Pedidos;
using HuB2B.WinService.Entidades.Universal;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;

namespace HuB2B.WinService.Data
{
    public class HuB2BDataContext:DbContext
    {
        public HuB2BDataContext()
            :base(@"Data Source=AMSAOCBRDB01;Initial Catalog=PORTAL_LOCCITANE;Persist Security Info=True;User ID=portalloccitane;Password=3u3NO483")
            //:base(@"Data Source=AMSAOTSQL01\CEGID;Initial Catalog=PORTAL_LOCCITANE;Persist Security Info=True;User ID=portalloccitane;Password=3u3NO483")
        { }

        public DbSet<ESTADO> Estado { get; set; }
        public DbSet<ESTABELECIMENTO> Estabelecimento { get; set; }
        public DbSet<MARCA_LOCCITANE> MarcaLoccitane { get; set; }
        public DbSet<MARCA_PRODUTOS> MarcaProdutos { get; set; }

        public DbSet<HB_ESTOQUE_CONTROLE> HbEstoqueControle { get; set; }
        public DbSet<HB_ESTOQUE_ITEM> HbEstoqueItem { get; set; }
        public DbSet<HB_ESTOQUE_RETORNO> HbEstoqueRetorno { get; set; }

        public DbSet<HB_PEDIDO_ID> HbPedidoId { get; set; }
        public DbSet<HB_PEDIDO_CAPA> HbPedidoCapa { get; set; }
        public DbSet<HB_PEDIDO_CLIENTE> HbPedidoCliente { get; set; }
        public DbSet<HB_PEDIDO_ENDERECO> HbPedidoEndereco { get; set; }
        public DbSet<HB_PEDIDO_ITEM> HbPedidoItem { get; set; }
        public DbSet<HB_PEDIDO_LOGISTICA> HbPedidoLogistica { get; set; }
        public DbSet<HB_PEDIDO_PAGAMENTO> HbPedidoPagamento { get; set; }
        public DbSet<HB_PEDIDO_RASTREAMENTO> HbPedidoRastreamento { get; set; }
        public DbSet<HB_PEDIDO_FATURADO> HbPedidoFaturado { get; set; }

        public DbSet<HB_RETORNO_LOG> HbRetornoLog { get; set; }
        public DbSet<HB_CANAL_VENDAS> HbCanalVendas { get; set; }
        public DbSet<HB_PARAMETROS> HbParametros { get; set; }

        public DbSet<HB_INTEGRACAO_OCMS> HbIntegracaoOCMS { get; set; }

        public DbSet<W_HB_ESTOQUE_PRODUTO> wHbEstoqueProduto { get; set; }
        public DbSet<W_HB_PEDIDO_FATURADO> wHbPedidoFaturado { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();

            modelBuilder.Entity<W_HB_ESTOQUE_PRODUTO>()
                .HasKey(a => new { a.COD_PRODUTO });

            modelBuilder.Entity<W_HB_PEDIDO_FATURADO>()
                .HasKey(a => new { a.PEDIDOID, a.PRODUTOID });

            modelBuilder.Entity<HB_INTEGRACAO_OCMS>()
                .HasKey(a=> new { a.OCMSID, a.NUM_PEDIDO});

            modelBuilder.Entity<HB_INTEGRACAO_OCMS>().Property(p => p.DATA_CADASTRO).HasColumnType("datetime").HasPrecision(0);
            modelBuilder.Entity<HB_INTEGRACAO_OCMS>().Property(p => p.DATA_ALTERACAO).HasColumnType("datetime").HasPrecision(0);


        }
    }
}
